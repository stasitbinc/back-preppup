<?php

namespace App\Nova\Actions;

use App\Models\Buyable;
use App\Models\Order;
use App\Models\User;
use App\Services\OrderService;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Select;

class DuplicateOrder extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param ActionFields $fields
     * @param Collection $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        /** @var Order $order */

        /** @var User $user */
        $service = app(OrderService::class);

        $date = Carbon::parse(Arr::get($fields, 'date'));

        $isRepeat = filter_var(Arr::get($fields, 'is_repeat'), FILTER_VALIDATE_BOOLEAN);

        $order = $models->first();

        $user = $order->user()->first();

        if (! $user->getAttribute('store_id')) {
            return Action::danger("You can't duplicate order for this client!");
        }

        $service->duplicateOrder($order, $date);

        if ($isRepeat) {
            $service->createOrder($order, $date->copy()->addWeek());
        }

        return Action::message(__('Order successfully duplicated!'));
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     * @throws Exception
     */
    public function fields()
    {
        return [
            Select::make('Date')
                ->rules('required')
                ->options(collect(CarbonPeriod::create(now(), now()->addMonth()))->filter(function (Carbon $date) {
                    return in_array($date->dayOfWeek, [1, 3, 5]);
                })->mapWithKeys(function (Carbon $date) {
                    return [$date->format('Y-m-d') => "{$date->format('d-m-Y')} ({$date->englishDayOfWeek})"];
                })->toArray())
                ->help(__('New Delivery Date')),

            Boolean::make('Repeat', 'is_repeat')
                ->help(__('Repeat this order the same day next week')),
        ];
    }
}
