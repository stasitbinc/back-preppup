<?php

namespace App\Nova;

use App\Enums\GenderEnum;
use App\Nova\Filters\UserOrders;
use Illuminate\Http\Request;
use Konekt\Enum\Enum;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
use R64\NovaFields\BelongsTo;
use Vyuldashev\NovaPermission\Role;

class User extends Resource
{
    public static function indexQuery(NovaRequest $request, $query)
    {
        $query->whereNull('store_id');

        return parent::indexQuery($request, $query);
    }

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\User::class;

    public function title()
    {
        return "{$this->getAttribute('name')} ({$this->getAttribute('type')})";
    }

    public static $group = 'Clients';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
        'age',
        'gender',
        'email',
        'address',
        'city',
        'floor',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Gravatar::make(),

            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),

            Number::make('Age')
                ->sortable()
                ->rules('min:1'),

            Select::make('Gender')
                ->options(GenderEnum::choices())
                ->displayUsingLabels()
                ->resolveUsing(function (Enum $value) {
                    return $value->value();
                })->displayUsing(function (Enum $value) {
                    return $value->label();
                }),

            Text::make('Address')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Phone')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Floor')
                ->hideFromIndex()
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('City')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            BelongsTo::make('Postcode'),

            Password::make('Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),

            HasMany::make('Orders'),

            HasMany::make('Subscriptions'),

            MorphToMany::make('Roles', 'roles', Role::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new UserOrders,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new DownloadExcel)->askForWriterType()->only('email'),
        ];
    }

    public static function label()
    {
        return __('Clients');
    }

    public static function singularLabel()
    {
        return __('Client');
    }
}
