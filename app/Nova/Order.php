<?php

namespace App\Nova;

use App\Enums\DeliveryEnum;
use App\Enums\OrderStatusEnum;
use App\Enums\PaymentTypeEnum;
use App\Enums\ReservationOrderEnum;
use App\Nova\Actions\CompleteOrder;
use App\Nova\Actions\DuplicateOrder;
use App\Nova\Actions\PayKorta;
use App\Nova\Actions\SendDeliveryList;
use App\Nova\Actions\SendIngredientsList;
use App\Nova\Actions\SendKitchenList;
use App\Nova\Actions\SendOrderStickers;
use App\Nova\Actions\SendUsersProductsList;
use App\Nova\Filters\DeliveryDates;
use App\Nova\Filters\OrderStatus;
use App\Nova\Metrics\OrdersAverageValue;
use App\Nova\Metrics\OrdersAverageValuePerDay;
use App\Nova\Metrics\OrdersNetSales;
use App\Nova\Metrics\OrdersNetSalesPerDay;
use App\Nova\Metrics\OrdersPerDay;
use App\Nova\Metrics\OrdersPerStatus;
use App\Nova\Metrics\OrdersTotal;
use Carbon\Carbon;
use Fourstacks\NovaCheckboxes\Checkboxes;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Konekt\Enum\Enum;
use Laravel\Nova\Fields\Badge;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use R64\NovaFields\Boolean;
use R64\NovaFields\Currency;
use R64\NovaFields\Date;
use R64\NovaFields\JSON;
use R64\NovaFields\Text;
use R64\NovaFields\Textarea;
use R64\NovaFields\Trix;
use Saumini\Count\RelationshipCount;

class Order extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Order::class;

    public function title()
    {
        return "#{$this->uuid} ({$this->status->label()})";
    }

    public static $group = '<span class="hidden">10</span>Orders';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'status',
        'note',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Heading::make('General'),

            ID::make()
                ->sortable()
                ->hideFromIndex(),

            Text::make('Order number', function () {
                return "#{$this->uuid}";
            })->detailLink(),

            Text::make('Client', function () {
                $user = \App\Models\User::query()->find($this->user_id);

                $type = Str::plural(Str::kebab($user->getAttribute('type')));

                $name = "{$user->getAttribute('name')} ({$user->getAttribute('type')})";

                return "<a class='no-underline dim text-primary font-bold' href='/admin/resources/{$type}/{$user->getKey()}'>$name</a>";
            })
                ->asHtml()
                ->exceptOnForms(),

            Select::make('Client', 'user_id')
                ->rules('required')
                ->displayUsingLabels()
                ->onlyOnForms()
                ->withMeta(['value' => $request->viaResourceId ?? $this->user_id])
                ->options(\App\Models\User::all()->mapWithKeys(function (\App\Models\User $user) {
                    return [$user->getKey() => "{$user->getAttribute('name')} ({$user->getAttribute('type')})"];
                }))
                ->readonly(function ($request) {
                    return $request->viaResourceId;
                }),

            BelongsTo::make('Coupon')
                ->hideFromIndex()
                ->nullable(),

            Badge::make('Status', function () {
                return $this->status->value();
            })->map([
                OrderStatusEnum::FAILED => 'danger',
                OrderStatusEnum::COMPLETED => 'success',
                OrderStatusEnum::PAID => 'info',
                OrderStatusEnum::PENDING => 'warning',
            ]),

            Select::make('Status')
                ->onlyOnForms()
                ->options(OrderStatusEnum::choices())
                ->displayUsingLabels()
                ->resolveUsing(function (Enum $value) {
                    return $value->value();
                })->displayUsing(function (Enum $value) {
                    return $value->label();
                })->rules('required'),

            Heading::make('Overview'),

            JSON::make('Overview', [
                Select::make('Reservations')
                    ->options(ReservationOrderEnum::choices())
                    ->displayUsingLabels()
                    ->rules('required'),

            ], 'overview')->flatten(),

            Heading::make('Allergy'),

            JSON::make('Allergy', [
                Boolean::make('Food intolerance / allergies', 'enabled'),

                Checkboxes::make('Allergies')
                    ->options(\App\Models\Allergy::all()
                        ->pluck('title', 'id')
                        ->toArray()
                    ),

                Textarea::make('Intolerance')
                    ->alwaysShow(),

            ], 'allergy')->flatten(),

            Heading::make('Delivery'),

            JSON::make('Delivery', [

                Select::make('Type')
                    ->options(DeliveryEnum::choices())
                    ->displayUsingLabels()
                    ->rules('required'),

                Textarea::make("If I'm not home", 'if')
                    ->rules('required')
                    ->alwaysShow(),

                Date::make('Date')
                    ->onlyOnForms()
                    ->format('DD-MM-YYYY'),

            ], 'delivery')->flatten(),

            Heading::make('Delivery Address')
                ->onlyOnDetail(),

            Boolean::make('The same as user address', 'is_same')
                ->onlyOnDetail()
                ->readOnly(),

            Text::make('Phone')
                ->onlyOnDetail()
                ->readOnly(),

            Text::make('Address', 'address_value')
                ->onlyOnDetail()
                ->readOnly(),

            Text::make('City')
                ->onlyOnDetail()
                ->readOnly(),

            Text::make('Floor')
                ->onlyOnDetail()
                ->readOnly(),

            Select::make('Postcode', 'postcode_id')
                ->onlyOnDetail()
                ->readOnly()
                ->options(\App\Models\Postcode::all()->pluck('zip', 'id')->toArray())
                ->displayUsingLabels(),

            Heading::make('Payment'),

            JSON::make('Payment', [

                Select::make('Type')
                    ->options(PaymentTypeEnum::choices())
                    ->displayUsingLabels()
                    ->rules('required'),

            ], 'payment')->flatten(),

            Heading::make('Amount'),

            JSON::make('Amount', [

                Currency::make('Total')
                    ->min(0)
                    ->displayUsing(function ($value) {
                        return "$value kr.";
                    })
                    ->rules('required'),

                Currency::make('Coupon')
                    ->min(0)
                    ->displayUsing(function ($value) {
                        return "$value kr.";
                    })
                    ->rules('required'),

                Currency::make('Shipping')
                    ->min(0)
                    ->displayUsing(function ($value) {
                        return "$value kr.";
                    })
                    ->rules('required'),

                Currency::make('Difference')
                    ->min(0)
                    ->displayUsing(function ($value) {
                        return "$value kr.";
                    })
                    ->rules('required'),

                Currency::make('For Payment')
                    ->min(0)
                    ->displayUsing(function ($value) {
                        return "$value kr.";
                    })
                    ->rules('required'),

            ], 'amount')->flatten(),

            HasMany::make('Deliveries'),

            HasMany::make('Buyables'),

            RelationshipCount::make('Order Items Count', 'buyables')
                ->hideFromIndex(),

            Heading::make('Delivery Date')
                ->onlyOnDetail(),

            Date::make('Delivery Date', function () use ($request) {
                $queryFilters = $request->query('filters');

                $decodedFilters = collect(json_decode(base64_decode($queryFilters), true));

                $filter = $decodedFilters->firstWhere('class', DeliveryDates::class);

                if ($filter && $filter['value']) {
                    return $this->deliveries()->where('date', $filter['value'])->first() ? $this->deliveries()->where('date', $filter['value'])->first()->getAttribute('date')->format('d-m-Y') : '-';
                } else {
                    return $this->deliveries()->exists() ? $this->deliveries()->first()->getAttribute('date')->format('d-m-Y') : Carbon::parse($this->getAttribute('delivery')->date)->format('d-m-Y');
                }
            }),

            Date::make('Created At')
                ->format('DD-MM-YYYY')
                ->onlyOnIndex(),

            Boolean::make('Has Meal Packs', function () {
                return (bool) $this->buyables()->where('buyable_type', 'MealPack')->count();
            })->exceptOnForms(),

            Heading::make('Additional Information'),

            Trix::make('Note')
                ->alwaysShow(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            (new OrdersTotal)->width('1/4'),
            (new OrdersNetSales)->width('1/4'),
            (new OrdersAverageValue)->width('1/4'),
            (new OrdersPerStatus)->width('1/4'),
            (new OrdersPerDay),
            (new OrdersNetSalesPerDay),
            (new OrdersAverageValuePerDay),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new OrderStatus,
            new DeliveryDates,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        $queryFilters = $request->query('filters');

        $decodedFilters = collect(json_decode(base64_decode($queryFilters), true));

        $filter = $decodedFilters->firstWhere('class', DeliveryDates::class);

        return [
            (new CompleteOrder)->confirmText(__('Are you sure want to complete this order?'))
                ->confirmButtonText(__('Complete'))
                ->cancelButtonText(__('Cancel')),

            ((new SendDeliveryList(Arr::get($filter, 'value', null)))->confirmText(__('Are you sure want to send delivery list?'))
                ->confirmButtonText(__('Send'))
                ->cancelButtonText(__('Cancel')))
                ->onlyOnIndex(),

            ((new SendKitchenList(Arr::get($filter, 'value', null)))->confirmText(__('Are you sure want to send kitchen list?'))
                ->confirmButtonText(__('Send'))
                ->cancelButtonText(__('Cancel')))
                ->onlyOnIndex(),

            ((new SendIngredientsList(Arr::get($filter, 'value', null)))->confirmText(__('Are you sure want to send ingredients list?'))
                ->confirmButtonText(__('Send'))
                ->cancelButtonText(__('Cancel')))
                ->onlyOnIndex(),

            ((new SendUsersProductsList(Arr::get($filter, 'value', null)))->confirmText(__('Are you sure want to send user products list?'))
                ->confirmButtonText(__('Send'))
                ->cancelButtonText(__('Cancel')))
                ->onlyOnIndex(),

            (new PayKorta)
                ->confirmButtonText(__('Charge'))
                ->onlyOnDetail()
                ->confirmText(__('Are you sure want to charge payment?')),

            (new DuplicateOrder)
                ->confirmButtonText(__('Duplicate'))
                ->onlyOnDetail(),

            new SendOrderStickers,
        ];
    }
}
