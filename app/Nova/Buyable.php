<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\Select;
use R64\NovaFields\Currency;
use R64\NovaFields\Number;
use R64\NovaFields\Text;

class Buyable extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Buyable::class;

    public function title()
    {
        return $this->buyable()->first()->getTranslation('name', 'is');
    }

    public static $displayInNavigation = false;

    public static $group = '<span class="hidden">10</span>Orders';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Select::make('Order number', 'order_id')
                ->rules('required')
                ->options(\App\Models\Order::all()->pluck('uuid', 'id'))
                ->onlyOnForms()
                ->withMeta(['value' => $request->viaResourceId])
                ->readonly(function ($request) {
                    return $request->viaResourceId;
                }),

            BelongsTo::make('Order number', 'order', \App\Nova\Order::class)
                ->exceptOnForms(),

            Text::make('Client', function () {
                return $this->order->user->name;
            }),

            Number::make('Quantity')
                ->rules('required')
                ->min(1),

            BelongsTo::make('Target', 'target', \App\Nova\Target::class),

            Currency::make('Price', function () {
                $value = $this->buyable ? $this->buyable->price : $this->price;

                return "$value kr.";
            }),

            HasMany::make('Subscriptions'),

            MorphTo::make('Buyable')->types([
                Product::class,
                MealPack::class,
            ])->rules('required'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
