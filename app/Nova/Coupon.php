<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use R64\NovaFields\Date;
use R64\NovaFields\Number;
use R64\NovaFields\Text;

class Coupon extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Coupon::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'title',
        'code',
        'amount',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Title')
                ->rules('required'),

            Text::make('Code')
                ->rules('required')
                ->creationRules('unique:coupons,code')
                ->updateRules('unique:coupons,code,{{resourceId}}'),

            Number::make('Meal Packs')
                ->min(0)
                ->max(100)
                ->displayUsing(function ($value) {
                    return "$value %";
                })
                ->rules('required', 'min:1', 'max:100'),

            Number::make('Products')
                ->min(0)
                ->max(100)
                ->displayUsing(function ($value) {
                    return "$value %";
                })
                ->rules('required', 'min:1', 'max:100'),

            Number::make('Max Products')
                ->min(1)
                ->rules('nullable', 'min:1'),

            Date::make('Expiration Date')
                ->rules('required'),

            Boolean::make('Is Enabled'),

            HasMany::make('Orders'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
