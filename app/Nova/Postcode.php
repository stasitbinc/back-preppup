<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use R64\NovaFields\Currency;
use R64\NovaFields\Number;

class Postcode extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Postcode::class;

    public static $group = 'Delivery';

    public function title()
    {
        return "{$this->getAttribute('zip')} ({$this->region->getAttribute('name')})";
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'zip',
        'standard',
        'extra',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Number::make('Zip')
                ->min(0)
                ->rules('required'),

            Currency::make(__('Free delivery availability'), 'free')
                ->help(__('Free delivery available for more than (leave empty if delivery is never free)'))
                ->min(1)
                ->displayUsing(function ($value) {
                    return "$value kr.";
                }),

            Currency::make(__('Delivery Price'), 'standard')
                ->min(1)
                ->displayUsing(function ($value) {
                    return "$value kr.";
                })
                ->rules('required'),

            /*Currency::make('Extra Delivery Price', 'extra')
                ->min(1)
                ->displayUsing(function ($value) {
                    return "$value kr.";
                })
                ->rules('required'),*/

            Boolean::make('Is standard delivery price?', 'is_standard'),

            Boolean::make('Is limited delivery?', 'is_limited')
                ->help(__('for 28 Day Meal Packs')),

            HasMany::make('Users'),

            BelongsTo::make('Region'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
