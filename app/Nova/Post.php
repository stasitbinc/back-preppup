<?php

namespace App\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use R64\NovaFields\BelongsTo;
use R64\NovaFields\Text;
use R64\NovaFields\Trix;
use Spatie\NovaTranslatable\Translatable;

class Post extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Post::class;

    public static $displayInNavigation = false;

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->getTranslation('title', 'is');
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'title',
        'description',
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Images::make('Main image', 'post')
                ->conversionOnIndexView('thumb')
                ->rules('required'),

            BelongsTo::make('Meal Pack Type'),

            Translatable::make([

                Text::make('Title')
                    ->sortable()
                    ->rules('required', 'max:255')
                    ->displayUsing(function () {
                        return $this->getTranslation('title', 'is');
                    }),

                Trix::make('Description')
                    ->sortable()
                    ->displayUsing(function () {
                        return $this->getTranslation('description', 'is');
                    }),

                Text::make('Name')
                    ->sortable()
                    ->rules('required', 'max:255')
                    ->displayUsing(function () {
                        return $this->getTranslation('name', 'is');
                    }),
            ]),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
