<?php

namespace App\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\ID;
use Preppup\IngredientsPicker\IngredientsPicker;
use R64\NovaFields\BelongsTo;
use R64\NovaFields\Currency;
use R64\NovaFields\JSON;
use R64\NovaFields\Number;
use R64\NovaFields\Text;
use R64\NovaFields\Trix;
use Spatie\NovaTranslatable\Translatable;

class Product extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Product::class;

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->getTranslation('name', 'is');
    }

    public static $group = '<span class="hidden">20</span>Products';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
        'price',
        'ingredients',
        'allergy',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Images::make('Main image', 'product')
                ->conversionOnIndexView('thumb'),

            Images::make('Images', 'product_gallery')
                ->conversionOnIndexView('thumb'),

            Translatable::make([
                Text::make('Name')
                    ->sortable()
                    ->rules('required', 'max:255')
                    ->displayUsing(function () {
                        return $this->getTranslation('name', 'is');
                    }),

                Trix::make('Ingredients')
                    ->rules('required')
                    ->alwaysShow()
                    ->sortable()
                    ->displayUsing(function () {
                        return $this->getTranslation('ingredients', 'is');
                    }),

                Trix::make('Allergy')
                    ->rules('required')
                    ->alwaysShow()
                    ->sortable()
                    ->displayUsing(function () {
                        return $this->getTranslation('allergy', 'is');
                    }),
            ]),

            Boolean::make('Is Hidden?', 'is_hidden'),

            Boolean::make('Show on the order page', 'is_additional'),

            BelongsTo::make('Type', 'product_type', \App\Nova\ProductType::class),

            IngredientsPicker::make('Product Ingredients'),

            BelongsToMany::make('Product Sizes')->fields(function () {
                return [
                    JSON::make('Dish', [
                        Number::make('Energy')
                            ->min(0)
                            ->rules('required')
                            ->displayUsing(function ($value) {
                                return "$value kcal";
                            }),

                        Number::make('Fat')
                            ->min(0)
                            ->step(0.1)
                            ->rules('required')
                            ->displayUsing(function ($value) {
                                return "$value g";
                            }),

                        Number::make('Carbohydrates')
                            ->min(0)
                            ->step(0.1)
                            ->rules('required')
                            ->displayUsing(function ($value) {
                                return "$value g";
                            }),

                        Number::make('Protein')
                            ->min(0)
                            ->step(0.1)
                            ->rules('required')
                            ->displayUsing(function ($value) {
                                return "$value g";
                            }),

                    ])->childConfig([
                        'labelClasses' => 'w-1/4 py-4 pr-4',
                        'fieldClasses' => 'w-2/5 py-4',
                    ]),

                    JSON::make('Sauce', [
                        Number::make('Energy')
                            ->min(0)
                            ->rules('required')
                            ->displayUsing(function ($value) {
                                return "$value kcal";
                            }),

                        Number::make('Fat')
                            ->min(0)
                            ->step(0.1)
                            ->rules('required')
                            ->displayUsing(function ($value) {
                                return "$value g";
                            }),

                        Number::make('Carbohydrates')
                            ->min(0)
                            ->step(0.1)
                            ->rules('required')
                            ->displayUsing(function ($value) {
                                return "$value g";
                            }),

                        Number::make('Protein')
                            ->min(0)
                            ->step(0.1)
                            ->rules('required')
                            ->displayUsing(function ($value) {
                                return "$value g";
                            }),

                    ])->childConfig([
                        'labelClasses' => 'w-1/4 py-4 pr-4',
                        'fieldClasses' => 'w-2/5 py-4',
                    ]),
                ];
            }),

            Currency::make('Price')
                ->min(1)
                ->displayUsing(function ($value) {
                    return "$value kr.";
                })
                ->rules('required'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
