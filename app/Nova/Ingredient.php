<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use R64\NovaFields\Number;

class Ingredient extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Ingredient::class;

    public static $group = '<span class="hidden">20</span>Ingredients';

    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
        'calories',
        'fat',
        'carbohydrates',
        'protein',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),

            Number::make('Calories')
                ->sortable()
                ->min(0)
                ->step(0.01)
                ->rules('required')
                ->displayUsing(function ($value) {
                    return "$value kcal";
                }),

            Number::make('Fat')
                ->sortable()
                ->min(0)
                ->step(0.01)
                ->rules('required')
                ->displayUsing(function ($value) {
                    return "$value g";
                }),

            Number::make('Carbohydrates')
                ->sortable()
                ->min(0)
                ->step(0.01)
                ->rules('required')
                ->displayUsing(function ($value) {
                    return "$value g";
                }),

            Number::make('Protein')
                ->sortable()
                ->min(0)
                ->step(0.01)
                ->rules('required')
                ->displayUsing(function ($value) {
                    return "$value g";
                }),

            BelongsTo::make('Category')
                ->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
