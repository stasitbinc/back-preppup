<?php

namespace App\Nova;

use App\Enums\SubscriptionStatusEnum;
use Illuminate\Http\Request;
use Konekt\Enum\Enum;
use Laravel\Nova\Fields\Badge;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;

class Subscription extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Subscription::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    public static $group = 'Clients';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            BelongsTo::make('Buyable'),

            BelongsTo::make('Order'),

            BelongsTo::make('Client', 'user', \App\Nova\User::class),

            Text::make('Attempts Count')
                ->exceptOnForms(),

            Badge::make('Status', function () {
                return $this->status->value();
            })->map([
                SubscriptionStatusEnum::CANCELED => 'danger',
                SubscriptionStatusEnum::ACTIVE => 'success',
            ]),

            Select::make('Status')
                ->options(SubscriptionStatusEnum::choices())
                ->displayUsingLabels()
                ->onlyOnForms()
                ->resolveUsing(function (Enum $value) {
                    return $value->value();
                })->displayUsing(function (Enum $value) {
                    return $value->label();
                })->rules('required'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
