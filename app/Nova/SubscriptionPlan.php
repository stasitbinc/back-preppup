<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use R64\NovaFields\Currency;
use R64\NovaFields\Number;
use R64\NovaFields\Text;

class SubscriptionPlan extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\SubscriptionPlan::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'days';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'days',
        'one_meal',
        'two_meal',
        'three_meal',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Number::make('Days')
                ->min(1)
                ->displayUsing(function ($value) {
                    return "$value days";
                })
                ->rules('required'),

            Number::make('Hours to save')
                ->min(1)
                ->displayUsing(function ($value) {
                    return "$value h.";
                })
                ->rules('required'),

            Currency::make('1 meal a day', 'one_meal')
                ->min(1)
                ->displayUsing(function ($value) {
                    return "$value kr.";
                })
                ->rules('required'),

            Currency::make('2 meal a day', 'two_meal')
                ->min(1)
                ->displayUsing(function ($value) {
                    return "$value kr.";
                })
                ->rules('required'),

            Currency::make('3 meal a day', 'three_meal')
                ->min(1)
                ->displayUsing(function ($value) {
                    return "$value kr.";
                })
                ->rules('required'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
