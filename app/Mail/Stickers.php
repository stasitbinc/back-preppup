<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Stickers extends Mailable
{
    use Queueable, SerializesModels;

    private $attachment;

    /**
     * Create a new notification instance.
     *
     * @param $attachment
     */
    public function __construct($attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.orders.products')
            ->subject('Clients Meals')
            ->attachData($this->attachment->output(), 'stickers.pdf', ['mime' => 'application/pdf']);
    }
}
