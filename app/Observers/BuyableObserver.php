<?php

namespace App\Observers;

use App\Models\Buyable;
use App\Models\Order;
use App\Services\OrderService;

class BuyableObserver
{
    private $service;

    public function __construct(OrderService $service)
    {
        $this->service = $service;
    }

    public function saved(Buyable $buyable)
    {
        /** @var Order $order */
        $order = $buyable->order()->first();

        $this->service->createDeliveryDates($order);
    }

    public function deleted(Buyable $buyable)
    {
        /** @var Order $order */
        $order = $buyable->order()->first();

        $this->service->createDeliveryDates($order);
    }
}
