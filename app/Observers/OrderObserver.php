<?php

namespace App\Observers;

use App\Jobs\SendOrderToSending;
use App\Models\Order;
use App\Models\Postcode;
use App\Models\Region;
use App\Sending\Client;
use App\Services\OrderService;
use Illuminate\Support\Str;

class OrderObserver
{
    private $service;

    private $client;

    public function __construct(OrderService $service, Client $client)
    {
        $this->service = $service;

        $this->client = $client;
    }

    public function saving(Order $order)
    {
        if (property_exists($order->getAttribute('payment'), 'korta')) {
            if (! $order->getAttribute('payment')->korta->token) {
                $order->forceFill([
                    'payment->korta->token' => strtolower(Str::random(19)),
                ]);
            }
        }
    }

    public function saved(Order $order)
    {
        /** @var Postcode $postcode */

        /** @var Region $region */
        $this->service->createDeliveryDates($order);

        /*$postcode = $order->postcode()->first();

        $region = $postcode->region()->first();

        if ($order->getAttribute('status')->isPaid() && $region->getAttribute('slug') == 'capital') {
            SendOrderToSending::dispatch($order);
        }*/
    }
}
