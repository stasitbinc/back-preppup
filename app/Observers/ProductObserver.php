<?php

namespace App\Observers;

use App\Models\Pivot\ProductTarget;
use App\Models\Product;

class ProductObserver
{
    public function saving(Product $product)
    {
        unset($product['product_ingredients']);
    }

    public function saved(Product $product)
    {
        if ($productIngredients = json_decode(request()->get('product_ingredients'))) {
            $this->setProductIngredientsAttribute($product, $productIngredients);
        }
    }

    public function setProductIngredientsAttribute(Product $product, $value)
    {
        $product->product_ingredients()->detach();

        collect($value)->map(function ($target) use ($product) {
            collect($target->categories)->map(function ($category) use ($target, $product) {
                collect($category->ingredients)->map(function ($ingredient) use ($category, $target, $product) {
                    if (property_exists($ingredient, 'is_enabled') && $ingredient->is_enabled) {
                        ProductTarget::query()->updateOrCreate(
                            [
                                'ingredient_id' => $ingredient->id,
                                'category_id' => $category->id,
                                'target_id' => $target->id,
                                'product_id' => $product->getKey(),
                            ],
                            [
                                'size' => $ingredient->size ?? 0,
                            ]
                        );
                    }
                });
            });
        });
    }
}
