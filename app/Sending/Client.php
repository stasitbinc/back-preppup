<?php

namespace App\Sending;

use App\Models\Delivery;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Str;
use Spatie\Geocoder\Facades\Geocoder;

class Client
{
    private $client;

    private $config;

    public function __construct(Guzzle $client, Config $config)
    {
        $this->client = $client;

        $this->config = $config;
    }

    public static function create(Config $config)
    {
        $stack = HandlerStack::create();

        $client = new Guzzle([
            'base_uri' => $config->getApiUrl(),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'handler' => $stack,
            'timeout' => 30,
        ]);

        return new static($client, $config);
    }

    public function getPrices(float $lat, float $lng)
    {
        $data = [
            'api_key' => $this->config->getApiKey(),
            'from_latitude' => 64.096939,
            'from_longitude' => -21.890596,
            'to_latitude' => $lat,
            'to_longitude' => $lng,
            'weight' => 1,
            'postcode' => 201,
        ];

        $packages = $this->request('shipping', ['query' => $data], 'GET');

        return head(data_get($packages, 'data'));
    }

    public function createOrder(Order $order)
    {
        /** @var User $user */
        $deliveries = $order->deliveries()->get();

        $user = $order->user()->first();

        $address = Geocoder::getCoordinatesForAddress("{$order->getAttribute('city')} {$order->getAttribute('address_value')}");

        $lat = data_get($address, 'lat');

        $lng = data_get($address, 'lng');

        if ($package = $this->getPrices($lat, $lng)) {
            $startPickupTime = Carbon::parse(data_get($package, 'start_pickup'))->toTimeString();

            $endDeliveryTime = Carbon::parse(data_get($package, 'end_delivery'))->toTimeString();

            foreach ($deliveries as $delivery) {
                /** @var Delivery $delivery */
                $deliveryDate = $delivery->getAttribute('date')->format('Y-m-d');

                $data = [
                    'api_key' => $this->config->getApiKey(),
                    'pickup_name' => __('PreppUp ehf'),
                    'pickup_address' => __('Hlíðarsmári 8'),
                    'pickup_city' => __('Reykjavík'),
                    'pickup_phone' => '5199887',
                    'pickup_zip' => 201,
                    'pickup_email' => 'info@preppup.is',
                    'pickup_latitude' => 64.096939,
                    'pickup_longitude' => -21.890596,
                    'shipping_name' => $user->getAttribute('name'),
                    'shipping_address' => "{$user->getAttribute('address')}, {$user->getAttribute('floor')}",
                    'shipping_city' => __('Reykjavík'),
                    'shipping_zip' => $order->postcode()->value('zip'),
                    'shipping_phone' => $this->checkUserPhone($user->getAttribute('phone')),
                    'shipping_latitude' => $lat,
                    'shipping_longitude' => $lng,
                    'shipping_id' => data_get($package, 'id', null),
                    'shipping_weight' => 1,
                    'pickup_time' => "$deliveryDate $startPickupTime",
                    'delivery_time' => "$deliveryDate $endDeliveryTime",
                    'note' => $order->getAttribute('delivery')->if,
                    'order_id' => $order->getKey(),
                ];

                $this->request('order/create', ['json' => $data], 'POST');
            }
        }
    }

    public function checkUserPhone(string $phone = null)
    {
        if (Str::startsWith($phone, '+354')) {
            return Str::replaceFirst('+354', '', $phone);
        }

        return $phone;
    }

    protected function request($endpoint, $params = [], $method = 'GET')
    {
        $response = $this->client->request($method, $endpoint, $params);

        return json_decode((string) $response->getBody(), true);
    }
}
