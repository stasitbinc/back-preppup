<?php

namespace App\Services;

use App\Enums\DayEnum;
use App\Enums\DaysEnum;
use App\Enums\DeliveryEnum;
use App\Enums\OrderStatusEnum;
use App\Enums\ReservationOrderEnum;
use App\Enums\SubscriptionStatusEnum;
use App\Enums\WeekEnum;
use App\Http\Requests\OrderRequest;
use App\Models\Buyable;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Coupon;
use App\Models\Delivery;
use App\Models\MealPack;
use App\Models\Order;
use App\Models\Pivot\MealPackProduct;
use App\Models\Postcode;
use App\Models\Region;
use App\Models\User;
use App\Notifications\PaymentErrorNotification;
use App\Notifications\PaymentNotification;
use App\Notifications\User\UserPaymentErrorNotification;
use App\Notifications\User\UserPaymentNotification;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;

class OrderService
{
    public function calculateOrderAmount(User $user, OrderRequest $request): array
    {
        /** @var Cart $cart */

        /** @var Postcode $postcode */

        /** @var Coupon $couponItem */
        $cart = $user->cart()->first();

        $amount = $cart->getAttribute('total');

        $totalProducts = $cart->getAttribute('total_products');

        $totalMealPacks = $cart->getAttribute('total_meal_packs');

        $postcode = $user->postcode()->first();

        if ($postcodeId = $request->input('address.postcode_id')) {
            $postcode = Postcode::query()->find($postcodeId);
        }

        $deliveryType = $request->input('delivery.type');

        $reservations = $request->input('overview.reservations');

        $shipping = 0;

        $coupon = 0;

        $difference = 0;

        $couponItem = null;

        if ($request->input('coupon_id') && $reservations === ReservationOrderEnum::ONCE) {
            $couponItem = Coupon::query()->find($request->input('coupon_id'));

            if ($this->checkCouponFree($couponItem)) {
                $coupon += $totalProducts + $totalMealPacks;
            } else {
                if ($totalProducts) {
                    $coupon += $totalProducts * $couponItem->getAttribute('products') / 100;
                }

                if ($totalMealPacks) {
                    $coupon += $totalMealPacks * $couponItem->getAttribute('meal_packs') / 100;
                }
            }

            $coupon = round($coupon);
        }

        $shipping = $this->getShippingAmount($shipping, $deliveryType, $postcode, $cart, $amount, $couponItem);

        if ($payDifference = $request->input('overview.pay_difference') && $amount < 4000) {
            $difference = 4000 - $amount;
        }

        $forPayment = $amount + $shipping - $coupon + $difference;

        if ($this->checkCouponFree($couponItem)) {
            $forPayment = 0;
        }

        return [
            'total' => $amount,
            'shipping' => $shipping,
            'coupon' => $coupon,
            'difference' => $difference,
            'for_payment' => $forPayment,
        ];
    }

    public function getShippingAmount(int $shipping, string $deliveryType, Postcode $postcode, Cart $cart, int $amount, Coupon $coupon = null): int
    {
        $free = $postcode->getAttribute('free');

        if ($free && ($amount > (int) $free)) {
            return 0;
        }

        if ($this->checkCouponFree($coupon)) {
            return 0;
        }

        if ($postcode->getAttribute('is_standard') && nova_get_setting('standard')) {
            $shipping += nova_get_setting('standard');

            return $shipping;
        }

        switch ($deliveryType) {
            case DeliveryEnum::DAY:
                $shipping += $postcode->getAttribute('standard');
                break;
            default:
                $shipping += $postcode->getAttribute('extra') * $cart->getAttribute('cart_items_count');
                break;
        }

        return $shipping;
    }

    public function connectItemsWithOrder(Order $order, Cart $cart, OrderRequest $request): void
    {
        $cartItems = $cart->cart_items()->get();

        $buyables = collect($request->get('buyables'));

        $ids = collect();

        $cartItems->map(function (CartItem $cartItem) use ($order, $cart, $ids) {
            $buyable = $order->buyables()->updateOrCreate([
                'buyable_type' => $cartItem->getAttribute('buyable_type'),
                'buyable_id' => $cartItem->getAttribute('buyable_id'),
                'target_id' => $cartItem->getAttribute('target_id'),
            ], [
                'price' => $cartItem->buyable()->first()->getAttribute('price') * $cartItem->getAttribute('quantity'),
                'quantity' => $cartItem->getAttribute('quantity'),
            ]);

            $ids->push($buyable->getKey());
        });

        $order->buyables()->whereNotIn('id', $ids)->delete();

        $buyables->map(function (array $item) {
            $buyable = Buyable::query()->find(Arr::get($item, 'id'));

            if ($buyable) {
                $buyable->update([
                    'date' => Arr::get($item, 'date'),
                ]);
            }
        });
    }

    public function subscribeUser(Order $order): void
    {
        /** @var User $user */
        $user = $order->user()->first();

        $buyables = $order->buyables()->where('buyable_type', 'MealPack')->get();

        $order->subscriptions()->delete();

        $buyables->map(function (Buyable $buyable) use ($user, $order) {
            /** @var MealPack $mealPack */
            $quantity = $buyable->getAttribute('quantity');

            for ($i = 0; $i < $quantity; $i++) {
                $buyable->subscriptions()->updateOrCreate([
                    'order_id' => $order->getKey(),
                    'user_id' => $user->getKey(),
                ], [
                    'status' => SubscriptionStatusEnum::ACTIVE,
                ]);
            }
        });
    }

    public function success(Order $order, array $request, User $user, $response, bool $isNotifyUser = null): void
    {
        $order->forceFill([
            'date' => Carbon::parse(Arr::get($request, 'time'))->format('Y-m-d'),
            'payment->korta->time' => Arr::get($request, 'time'),
            'payment->korta->downloadmd5' => Arr::get($request, 'downloadmd5'),
            'payment->korta->authcode' => Arr::get($request, 'authcode'),
            'payment->korta->cardbrand' => Arr::get($request, 'cardbrand'),
            'payment->korta->card4' => Arr::get($request, 'card4'),
            'payment->done' => true,
            'status' => OrderStatusEnum::PAID,
        ])->save();

        $user->cart_items()->delete();

        if ($order->buyables()->where('buyable_type', 'MealPack')->exists() && $order->getAttribute('overview')->reservations == ReservationOrderEnum::SUBSCRIBE) {
            $this->subscribeUser($order);
        }

        $users = User::query()->role('Admin')->get();

        Notification::send($users, new PaymentNotification($response, $order));

        Notification::route('mail', 'pantanir@preppup.is')
            ->notify((new PaymentNotification($response, $order))->delay(now()->addMinute()));

        if ($isNotifyUser) {
            $user->notify((new UserPaymentNotification($order))
                ->locale($user->language ? $user->language->language : 'is'));
        }
    }

    public function error(Order $order, User $user, $response, bool $isNotifyUser = null): void
    {
        $order->forceFill([
            'payment->done' => false,
            'status' => OrderStatusEnum::FAILED,
        ])->save();

        $users = User::query()->role('Admin')->get();

        Notification::send($users, new PaymentErrorNotification($response, $order));

        if ($isNotifyUser) {
            $user->notify((new UserPaymentErrorNotification($order))
                ->locale($user->language ? $user->language->language : 'is'));
        }

        abort(403, __('Payment failed.'));
    }

    public function confirmFreeOrder(Order $order): void
    {
        /** @var User $user */
        $user = $order->user()->first();

        $order->forceFill([
            'date' => now(),
            'payment->done' => true,
            'status' => OrderStatusEnum::PAID,
        ])->save();

        $user->cart_items()->delete();

        $users = User::query()->role('Admin')->get();

        $response = [];

        Notification::send($users, new PaymentNotification($response, $order));

        Notification::route('mail', 'pantanir@preppup.is')
            ->notify((new PaymentNotification($response, $order))->delay(now()->addMinute()));

        $user->notify((new UserPaymentNotification($order))
            ->locale($user->language ? $user->language->language : 'is'));
    }

    public function checkCouponFree(Coupon $coupon = null): bool
    {
        if (is_null($coupon)) {
            return false;
        }

        $products = $coupon->getAttribute('products');

        $mealPacks = $coupon->getAttribute('meal_packs');

        return $products == 100 || $mealPacks == 100;
    }

    public function createDeliveryDates(Order $order, Carbon $date = null, bool $isCleanOld = true): void
    {
        if ($isCleanOld) {
            $order->deliveries()->delete();
        }

        if ($order->getAttribute('status')->isPaid()) {
            $deliveryDate = Carbon::parse($order->getAttribute('delivery')->date);

            if ($date) {
                $deliveryDate = $date;
            }

            $product = $order->products()->first();

            $mealPacks = $order->meal_packs()->get();

            if ($mealPacks->count()) {
                $mealPacks->map(function (MealPack $mealPack) use ($deliveryDate, $order) {
                    $whichDay = $mealPack->products()->first()->pivot->day->value();

                    if ($whichDay == DayEnum::ONE_TIME) {
                        $order->deliveries()->updateOrCreate([
                            'date' => $deliveryDate->copy()->format('Y-m-d'),
                        ]);
                    } else {
                        $this->checkFirstAndNextDay($mealPack, $order, $deliveryDate);
                    }
                });
            } elseif ($product && $deliveryDate) {
                $order->deliveries()->updateOrCreate([
                    'date' => $deliveryDate->format('Y-m-d'),
                ]);
            }
        }
    }

    public function checkFirstAndNextDay(MealPack $mealPack, Order $order, Carbon $deliveryDate): void
    {
        $dayOfWeek = strtolower($deliveryDate->copy()->englishDayOfWeek);

        switch ($dayOfWeek) {
            case DaysEnum::MONDAY:
                $firstDay = DaysEnum::MONDAY;
                $nextDay = DaysEnum::THURSDAY;
                $currentWeek = true;
                break;
            case DaysEnum::TUESDAY:
                $firstDay = DaysEnum::TUESDAY;
                $nextDay = DaysEnum::FRIDAY;
                $currentWeek = true;
                break;
            case DaysEnum::WEDNESDAY:
                $firstDay = DaysEnum::WEDNESDAY;
                $nextDay = DaysEnum::SUNDAY;
                $currentWeek = true;
                break;
            case DaysEnum::THURSDAY:
                $firstDay = DaysEnum::THURSDAY;
                $nextDay = DaysEnum::MONDAY;
                $currentWeek = false;
                break;
            case DaysEnum::FRIDAY:
                $firstDay = DaysEnum::FRIDAY;
                $nextDay = DaysEnum::TUESDAY;
                $currentWeek = false;
                break;
            default:
                $firstDay = DaysEnum::SUNDAY;
                $nextDay = DaysEnum::WEDNESDAY;
                $currentWeek = false;
                break;
        }

        if (! $currentWeek) {
            $this->createDeliveriesAndAttachMealPackProducts($mealPack, $order, $deliveryDate, $firstDay, false);

            $this->createDeliveriesAndAttachMealPackProducts($mealPack, $order, $deliveryDate, $nextDay, true);

            $deliveryDate = $deliveryDate->copy()->next($firstDay);

            for ($i = 0; $i < $mealPack->getAttribute('weeks') - 1; $i++) {
                $this->createDeliveriesAndAttachMealPackProducts($mealPack, $order, $deliveryDate, $firstDay, false);

                $this->createDeliveriesAndAttachMealPackProducts($mealPack, $order, $deliveryDate, $nextDay, true);

                $deliveryDate = $deliveryDate->addWeek();
            }
        } else {
            $this->createDeliveriesAndAttachMealPackProducts($mealPack, $order, $deliveryDate, $firstDay, false);

            $this->createDeliveriesAndAttachMealPackProducts($mealPack, $order, $deliveryDate, $nextDay, true);

            for ($i = 0; $i < $mealPack->getAttribute('weeks') - 1; $i++) {
                $this->createDeliveriesAndAttachMealPackProducts($mealPack, $order, $deliveryDate, $firstDay, true);

                $this->createDeliveriesAndAttachMealPackProducts($mealPack, $order, $deliveryDate, $nextDay, true);

                $deliveryDate->addWeek();
            }
        }
    }

    public function createDeliveriesAndAttachMealPackProducts(MealPack $mealPack, Order $order, Carbon $deliveryDate, string $day, bool $isIncrease): void
    {
        /** @var Delivery $delivery */
        $mealPackProducts = $this->getMealPackProducts($mealPack, $this->checkDay($day));

        $date = $deliveryDate->copy()->format('Y-m-d');

        if ($isIncrease) {
            $date = $deliveryDate->copy()->next($day)->format('Y-m-d');
        }

        $delivery = $order->deliveries()->create(compact('date'));

        $delivery->meal_pack_products()->sync($mealPackProducts->pluck('id')->toArray());
    }

    public function checkDay(string $day): string
    {
        switch ($day) {
            case DaysEnum::SUNDAY:
            case DaysEnum::MONDAY:
            case DaysEnum::TUESDAY:
                return DaysEnum::MONDAY;
            default:
                return DaysEnum::FRIDAY;
        }
    }

    public function getMealPackProducts(MealPack $mealPack, string $day): Collection
    {
        return MealPackProduct::query()
            ->where('meal_pack_id', $mealPack->getKey())
            ->whereIn('product_id', $mealPack->products()->get()->pluck('id')->toArray())
            ->where('day', $day)
            ->get();
    }

    public function duplicateOrder(Order $order, Carbon $date): void
    {
        /** @var User $user */

        /** @var Order $newOrder */
        $user = $order->user()->first();

        $buyables = $order->buyables()->get();

        $newOrder = $order->forceFill([
            'delivery->date' => $date->format('Y-m-d'),
        ]);

        $newOrder = $user->orders()->create($newOrder->toArray());

        $buyables->map(function (Buyable $buyable) use ($newOrder) {
            $newOrder->buyables()->updateOrCreate([
                'buyable_type' => $buyable->getAttribute('buyable_type'),
                'buyable_id' => $buyable->getAttribute('buyable_id'),
                'target_id' => $buyable->getAttribute('target_id'),
            ], [
                'price' => $buyable->getAttribute('price'),
                'quantity' => $buyable->getAttribute('quantity'),
            ]);
        });
    }
}
