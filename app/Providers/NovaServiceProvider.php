<?php

namespace App\Providers;

use App\Models\User;
use App\Nova\Metrics\ActiveSubscriptions;
use App\Nova\Metrics\NewUsers;
use App\Nova\Metrics\UsersPerDay;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;
use OptimistDigital\NovaSettings\NovaSettings;
use R64\NovaFields\Currency;
use Spatie\NovaTranslatable\Translatable;
use Vyuldashev\NovaPermission\NovaPermissionTool;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Translatable::defaultLocales(['en', 'is']);

        NovaSettings::addSettingsFields([
            Currency::make(__('Standard delivery price'), 'standard')
                ->min(1)
                ->displayUsing(function ($value) {
                    return "$value kr.";
                }),
        ]);

        Nova::serving(function (ServingNova $event) {
            $this->app->setLocale('en');
        });

        parent::boot();
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            /** @var User $user */
            return $user->hasRole('Admin');
        });
    }

    /**
     * Get the cards that should be displayed on the default Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            new NewUsers,
            new UsersPerDay,
            new ActiveSubscriptions,
        ];
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function dashboards()
    {
        return [];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
            NovaPermissionTool::make(),
            new NovaSettings,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function resources()
    {
        Nova::resources([
            \App\Nova\Order::class,
            \App\Nova\Buyable::class,
            \App\Nova\SubscriptionPlan::class,
            \App\Nova\Allergy::class,
            \App\Nova\Coupon::class,
            \App\Nova\FaqCategory::class,
            \App\Nova\Faq::class,
            \App\Nova\User::class,
            \App\Nova\Subscription::class,
            \App\Nova\MealPack::class,
            \App\Nova\MealPackGroup::class,
            \App\Nova\Delivery::class,
            \App\Nova\MealPackProduct::class,
            \App\Nova\MealPackInfo::class,
            \App\Nova\MealPackTarget::class,
            \App\Nova\Post::class,
            \App\Nova\Postcode::class,
            \App\Nova\Term::class,
            \App\Nova\Product::class,
            \App\Nova\ProductSize::class,
            \App\Nova\ProductType::class,
            \App\Nova\Target::class,
            \App\Nova\Category::class,
            \App\Nova\Ingredient::class,
            \App\Nova\Region::class,
            \App\Nova\Store::class,
            \App\Nova\StoreLocation::class,
        ]);
    }
}
