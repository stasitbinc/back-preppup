<?php

namespace App\Console\Commands;

use App\Jobs\SendContactsToActiveCampaign;
use App\Models\User;
use Illuminate\Console\Command;

class SendContacts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:contacts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        User::query()->chunk(10, function ($users) {
            SendContactsToActiveCampaign::dispatch(collect($users));
        });
    }
}
