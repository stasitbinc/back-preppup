<?php

namespace App\Console\Commands;

use App\Jobs\CheckRepeatOrders;
use App\Models\User;
use Illuminate\Console\Command;

class RepeatOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repeat:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        User::query()
            ->whereNotNull('store_id')
            ->where('is_repeat', true)
            ->chunk(100, function ($users) {
                CheckRepeatOrders::dispatch(collect($users));
            });
    }
}
