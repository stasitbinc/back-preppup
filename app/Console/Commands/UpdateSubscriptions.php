<?php

namespace App\Console\Commands;

use App\Enums\SubscriptionStatusEnum;
use App\Jobs\CheckUserSubscriptions;
use App\Models\Subscription;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class UpdateSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Subscriptions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        Subscription::query()
            ->where('status', SubscriptionStatusEnum::ACTIVE)
            ->chunk(100, function ($subscriptions) {
                CheckUserSubscriptions::dispatch(collect($subscriptions));
            });
    }
}
