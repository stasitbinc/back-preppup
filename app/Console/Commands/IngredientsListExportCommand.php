<?php

namespace App\Console\Commands;

use App\Enums\OrderStatusEnum;
use App\Jobs\CheckAndExportIngredientsList;
use App\Models\Order;
use Illuminate\Console\Command;

class IngredientsListExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ingredients:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        Order::query()
            ->where('status', OrderStatusEnum::PAID)
            ->chunk(100, function ($orders) {
                CheckAndExportIngredientsList::dispatch(collect($orders));
            });
    }
}
