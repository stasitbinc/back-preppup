<?php

namespace App\Console\Commands;

use App\Jobs\SendOrderToSending as SendOrderToSendingJob;
use App\Models\Order;
use App\Models\Postcode;
use App\Models\Region;
use App\Sending\Client;
use Illuminate\Console\Command;

class SendOrderToSending extends Command
{
    private $client;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:order {order}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        /** @var Order $order */

        /** @var Postcode $postcode */

        /** @var Region $region */
        $order = Order::query()->findOrFail($this->argument('order'));

        $postcode = $order->postcode()->first();

        $region = $postcode->region()->first();

        if ($order->getAttribute('status')->isPaid() && $region->getAttribute('slug') == 'capital') {
            SendOrderToSendingJob::dispatch($order);
        }
    }
}
