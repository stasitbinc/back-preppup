<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('telescope:prune')->daily();

        $schedule->command('subscriptions:sync')
            ->dailyAt('00:01')
            ->withoutOverlapping();

        $schedule->command('backup:run --only-db')
            ->daily();

        $schedule->command('delivery-list:export')
            ->daily();

        $schedule->command('kitchen:export')
            ->daily();

        $schedule->command('ingredients:export')
            ->daily();

        $schedule->command('user:products')
            ->daily();

        $schedule->command('repeat:orders')
            ->daily();

        $schedule->command('log:clear')
            ->weekly();

        $schedule->command('clean:carts')
            ->monthly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
