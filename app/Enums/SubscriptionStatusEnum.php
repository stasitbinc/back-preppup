<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class SubscriptionStatusEnum extends Enum
{
    const __default = self::ACTIVE;

    const ACTIVE = 'active';
    const CANCELED = 'canceled';

    public static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::ACTIVE => __('Active'),
            self::CANCELED => __('Canceled'),
        ];
    }
}
