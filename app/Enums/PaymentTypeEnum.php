<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class PaymentTypeEnum extends Enum
{
    const __default = self::EMPTY;

    const EMPTY = '-';
    const KORTA = 'korta';
    const PEI = 'pei';
    const NETGIRO = 'netgiro';

    public static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::KORTA => __('Korta'),
            self::PEI => __('Pei'),
            self::NETGIRO => __('Netgiro'),
        ];
    }
}
