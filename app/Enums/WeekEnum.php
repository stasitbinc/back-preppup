<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class WeekEnum extends Enum
{
    const __default = self::FIRST;

    const FIRST = 'first';
    const SECOND = 'second';
    const THIRD = 'third';
    const FOURTH = 'fourth';

    public static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::FIRST => __('First'),
            self::SECOND => __('Second'),
            self::THIRD => __('Third'),
            self::FOURTH => __('Fourth'),
        ];
    }
}
