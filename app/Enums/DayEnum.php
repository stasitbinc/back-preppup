<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class DayEnum extends Enum
{
    const __default = self::ONE_TIME;

    const ONE_TIME = 'one_time';
    const MONDAY = 'monday';
    const FRIDAY = 'friday';

    public static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::ONE_TIME => __('One-time delivery'),
            self::MONDAY => __('Day 1 (less products)'),
            self::FRIDAY => __('Day 2 (more products)'),
        ];
    }
}
