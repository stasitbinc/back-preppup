<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class GenderEnum extends Enum
{
    const __default = self::MEN;

    const MEN = 'men';
    const WOMEN = 'women';
    const OTHER = 'other';

    public static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::MEN => __('Men'),
            self::WOMEN => __('Women'),
            self::OTHER => __('Other'),
        ];
    }
}
