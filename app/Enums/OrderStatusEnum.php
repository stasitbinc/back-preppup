<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class OrderStatusEnum extends Enum
{
    const __default = self::PENDING;

    const PENDING = 'pending';
    const PAID = 'paid';
    const COMPLETED = 'completed';
    const FAILED = 'failed';

    public static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::PENDING => __('Pending'),
            self::PAID => __('Paid'),
            self::COMPLETED => __('Completed'),
            self::FAILED => __('Failed'),
        ];
    }
}
