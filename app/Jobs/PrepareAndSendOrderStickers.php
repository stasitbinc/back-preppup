<?php

namespace App\Jobs;

use App\Mail\Stickers;
use App\Models\MealPack;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class PrepareAndSendOrderStickers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $orders;

    /**
     * Create a new job instance.
     *
     * @param Collection $orders
     */
    public function __construct(Collection $orders)
    {
        $this->orders = $orders;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $products = collect();

        $this->orders->map(function (Order $order) use ($products) {
            if ($order->getAttribute('status')->isPaid()) {
                $order->products()->get()->map(function (Product $product) use ($products) {
                    $product->setAttribute('target_id', $product->pivot->target_id);

                    $products->push($product);
                });

                $mealPacks = $order->meal_packs()->get();

                $mealPacks->map(function (MealPack $mealPack) use ($products) {
                    $mealPack->products()->get()->map(function (Product $product) use ($products, $mealPack) {
                        $product->setAttribute('target_id', $mealPack->getAttribute('target_id'));

                        $products->push($product);
                    });
                });
            }
        });

        $pdf = \PDF::loadView('stickers', compact('products'))->setPaper('a5');

        $users = User::query()->role('Admin')->get();

        $users->map(function (User $user) use ($pdf) {
            Mail::to($user)->send(new Stickers($pdf));
        });
    }
}
