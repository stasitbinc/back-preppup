<?php

namespace App\Jobs;

use App\Enums\DayEnum;
use App\Exports\OrdersKitchenExport;
use App\Mail\KitchenListExport;
use App\Models\Buyable;
use App\Models\Delivery;
use App\Models\MealPack;
use App\Models\Order;
use App\Models\Pivot\MealPackProduct;
use App\Models\Product;
use App\Models\Target;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Excel as BaseExcel;
use Maatwebsite\Excel\Facades\Excel;

class CheckAndExportKitchenList implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $orders;

    protected $date;

    /**
     * Create a new job instance.
     *
     * @param Collection $orders
     * @param Carbon|null $date
     */
    public function __construct(Collection $orders, Carbon $date = null)
    {
        $this->orders = $orders;

        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->date) {
            $this->exportKitchenList($this->date);
        } else {
            $this->exportKitchenList(now()->addDays(2));

            /*Carbon::setLocale('en');

            switch (now()->format('l')) {
                case 'Wednesday':
                case 'Monday':
                    $this->exportKitchenList(now()->addDays(3));
                    break;
                case 'Friday':
                    $this->exportKitchenList(now()->addDays(4));
                    break;
            }*/
        }
    }

    public function exportKitchenList(Carbon $date)
    {
        $allProducts = collect();

        $this->orders = $this->orders->filter(function (Order $order) use ($date) {
            return $order->deliveries()->whereDate('date', $date->format('Y-m-d'))->exists();
        });

        $this->orders->map(function (Order $order) use ($allProducts, $date) {
            $products = $order->products()->get();

            /** @var Delivery $delivery */
            $delivery = $order->deliveries()->whereDate('date', $date->format('Y-m-d'))->first();

            $products->map(function (Product $product) use ($allProducts, $order) {
                $product->setAttribute('target_id', $product->pivot->target_id);

                $quantity = Buyable::query()
                    ->where('order_id', $order->getKey())
                    ->where('buyable_type', 'Product')
                    ->where('buyable_id', $product->getKey())
                    ->first()
                    ->getAttribute('quantity');

                for ($i = 0; $i < $quantity; $i++) {
                    $allProducts->push($product);
                }
            });

            $mealPacks = $order->meal_packs()->get();

            $mealPacks->map(function (MealPack $mealPack) use ($allProducts, $delivery, $date, $order) {
                $quantity = Buyable::query()
                    ->where('order_id', $order->getKey())
                    ->where('buyable_type', 'MealPack')
                    ->where('buyable_id', $mealPack->getKey())
                    ->first()
                    ->getAttribute('quantity');

                $whichDay = $mealPack->products()->first()->pivot->day->value();

                if ($whichDay == DayEnum::ONE_TIME) {
                    $products = $mealPack->products()->wherePivot('day', DayEnum::ONE_TIME)->get();
                } else {
                    $products = collect();

                    $delivery->meal_pack_products()->get()->map(function (MealPackProduct $mealPackProduct) use ($products) {
                        $products->push($mealPackProduct->product()->first());
                    });
                }

                for ($i = 0; $i < $quantity; $i++) {
                    $products->map(function (Product $product) use ($allProducts, $mealPack) {
                        $product->setAttribute('target_id', $mealPack->getAttribute('target_id'));

                        $allProducts->push($product);
                    });
                }
            });
        });

        $grouped = $allProducts->groupBy([
            function (Product $product) {
                return $product->product_type()->first()->getTranslation('title', 'is');
            },
            function (Product $product) {
                return $product->getTranslation('name', 'is');
            },
        ], $preserveKeys = true);

        $grouped = $grouped->map(function (Collection $typeGroup) {
            $typeGroup = $typeGroup->map(function (Collection $productsGroup, string $key) {
                $productTypeWithTargets = $productsGroup->first()->product_type()->first()->getAttribute('with_targets');

                if ($productTypeWithTargets) {
                    $productsGroup = $productsGroup->groupBy(function (Product $product) {
                        /** @var Target $target */
                        $target = Target::query()->find($product->getAttribute('target_id') ?? rand(1, 3));

                        return str_replace('-', '_', $target->getAttribute('slug'));
                    });

                    $productsGroup = $productsGroup->map(function (Collection $group) {
                        return $group->count();
                    });

                    $targets = Target::all();

                    $targets->map(function (Target $target) use ($productsGroup) {
                        if (! $productsGroup->contains(function ($value, $key) use ($target) {
                            return str_replace('-', '_', $target->getAttribute('slug')) == $key;
                        })) {
                            $productsGroup->put(str_replace('-', '_', $target->getAttribute('slug')), '0');
                        }
                    });

                    $productsGroup->put('name', $key);
                } else {
                    $productsGroup = $productsGroup->mapWithKeys(function (Product $product) use ($productsGroup, $key) {
                        return [
                            'count' => $productsGroup->count(),
                            'name' => $key,
                        ];
                    });
                }

                return $productsGroup;
            });

            return $typeGroup;
        });

        $filename = "Kitchen List ({$date->format('l d.F')}).xlsx";

        $attachment = Excel::raw(new OrdersKitchenExport($grouped, $date), BaseExcel::XLSX);

//        Storage::disk('google')->put($filename, $attachment);

        $users = User::query()->role('Admin')->get();

        $users->map(function (User $user) use ($attachment, $filename, $date) {
            Mail::to($user)->send(new KitchenListExport($attachment, $filename, $date));
        });
    }
}
