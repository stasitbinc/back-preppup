<?php

namespace App\Jobs;

use App\Enums\DayEnum;
use App\Exports\OrdersIngredientsExport;
use App\Mail\IngredientsListExport;
use App\Models\Buyable;
use App\Models\Delivery;
use App\Models\Ingredient;
use App\Models\MealPack;
use App\Models\Order;
use App\Models\Pivot\MealPackProduct;
use App\Models\Product;
use App\Models\Target;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Excel as BaseExcel;
use Maatwebsite\Excel\Facades\Excel;

class CheckAndExportIngredientsList implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $orders;

    protected $date;

    /**
     * Create a new job instance.
     *
     * @param Collection $orders
     * @param Carbon|null $date
     */
    public function __construct(Collection $orders, Carbon $date = null)
    {
        $this->orders = $orders;

        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->date) {
            $this->exportIngredients($this->date);
        } else {
            $this->exportIngredients(now()->addDays(2));

            /*Carbon::setLocale('en');

            switch (now()->format('l')) {
                case 'Wednesday':
                case 'Monday':
                    $this->exportIngredients(now()->addDays(3));
                    break;
                case 'Friday':
                    $this->exportIngredients(now()->addDays(4));
                    break;
            }*/
        }
    }

    public function exportIngredients(Carbon $date)
    {
        $ingredients = collect();

        $this->orders = $this->orders->filter(function (Order $order) use ($date) {
            return $order->deliveries()->whereDate('date', $date->format('Y-m-d'))->exists();
        });

        if ($this->orders->count()) {
            $this->orders->map(function (Order $order) use ($ingredients, $date) {

                /** @var Delivery $delivery */
                $delivery = $order->deliveries()->whereDate('date', $date->format('Y-m-d'))->first();

                $products = $order->products()->get();

                $products->map(function (Product $product) {
                    $product->setAttribute('target_id', $product->pivot->target_id);
                });

                $mealPacks = $order->meal_packs()->get();

                $products->map(function (Product $product) use ($ingredients, $order) {
                    $quantity = Buyable::query()
                        ->where('order_id', $order->getKey())
                        ->where('buyable_type', 'Product')
                        ->where('buyable_id', $product->getKey())
                        ->first()
                        ->getAttribute('quantity');

                    for ($i = 0; $i < $quantity; $i++) {
                        $productIngredients = $product->product_ingredients()->get();

                        $productIngredients->map(function (Ingredient $ingredient) use ($ingredients, $product) {
                            $ingredient->setAttribute('target_id', $product->getAttribute('target_id') ?? Target::query()->inRandomOrder()->first()->getKey());

                            $ingredients->push($ingredient);
                        });
                    }
                });

                $mealPacks->map(function (MealPack $mealPack) use ($ingredients, $delivery, $order) {
                    $quantity = Buyable::query()
                        ->where('order_id', $order->getKey())
                        ->where('buyable_type', 'MealPack')
                        ->where('buyable_id', $mealPack->getKey())
                        ->first()
                        ->getAttribute('quantity');

                    $whichDay = $mealPack->products()->first()->pivot->day->value();

                    if ($whichDay == DayEnum::ONE_TIME) {
                        $products = $mealPack->products()->wherePivot('day', DayEnum::ONE_TIME)->get();
                    } else {
                        $products = collect();

                        $delivery->meal_pack_products()->get()->map(function (MealPackProduct $mealPackProduct) use ($products) {
                            $products->push($mealPackProduct->product()->first());
                        });
                    }

                    $products->map(function (Product $product) use ($ingredients, $mealPack, $quantity) {
                        $productIngredients = $product->product_ingredients()->get();

                        for ($i = 0; $i < $quantity; $i++) {
                            $productIngredients->map(function (Ingredient $ingredient) use ($ingredients, $mealPack) {
                                $ingredient->setAttribute('target_id', $mealPack->getAttribute('target_id') ?? Target::query()->inRandomOrder()->first()->getKey());

                                $ingredients->push($ingredient);
                            });
                        }
                    });
                });
            });

            $grouped = $ingredients->groupBy([
                function (Ingredient $ingredient) {
                    return $ingredient->category()->first()->getAttribute('name');
                },
                function (Ingredient $ingredient) {
                    return $ingredient->getAttribute('name');
                },
                function (Ingredient $ingredient) {
                    $target = Target::query()->find($ingredient->getAttribute('target_id'));

                    return str_replace('-', '_', $target->getAttribute('slug'));
                },
            ], $preserveKeys = true);

            $grouped = $grouped->map(function (Collection $categoryGroup, string $category) {
                $categoryGroup = $categoryGroup->map(function (Collection $ingredientGroup, string $key) use ($category) {
                    $ingredientGroup = $ingredientGroup->map(function (Collection $targetGroup) {
                        /** @var Ingredient $ingredient */
                        $ingredient = $targetGroup->first();

                        return [
                            'count' => $targetGroup->count(),
                            'calculated' => $targetGroup->count() * $ingredient->pivot->size,
                        ];
                    });

                    /*$targets = Target::all();

                    $targets->map(function (Target $target) use ($ingredientGroup) {
                        if (!$ingredientGroup->contains(function ($value, $key) use ($target) {
                            return str_replace('-', '_', $target->getAttribute('slug')) == $key;
                        })) {
                            $ingredientGroup->put(str_replace('-', '_', $target->getAttribute('slug')), ['count' => '0', 'calculated' => 0]);
                        }
                    });*/

                    $total = 0;

                    foreach ($ingredientGroup as $target) {
                        $total += Arr::get($target, 'calculated');
                    }

                    $count = 0;

                    foreach ($ingredientGroup as $target) {
                        $count += Arr::get($target, 'count', 0);
                    }

                    $ingredientGroup->put('count', $count);

                    $ingredientGroup->put('name', $key);
                    $ingredientGroup->put('total', $total);

                    return $ingredientGroup;
                });

                return $categoryGroup;
            });

            $filename = "Ingredients List ({$date->format('l d.F')}).xlsx";

            $attachment = Excel::raw(new OrdersIngredientsExport($grouped, $date), BaseExcel::XLSX);

//        Storage::disk('google')->put($filename, $attachment);

            $users = User::query()->role('Admin')->get();

            $users->map(function (User $user) use ($attachment, $filename, $date) {
                Mail::to($user)->send(new IngredientsListExport($attachment, $filename, $date));
            });
        }
    }
}
