<?php

namespace App\Jobs;

use App\Enums\DayEnum;
use App\Exports\UsersProductsExport;
use App\Mail\UsersProductsExport as ProductsExport;
use App\Models\Buyable;
use App\Models\Delivery;
use App\Models\MealPack;
use App\Models\Order;
use App\Models\Pivot\MealPackProduct;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel as BaseExcel;
use Maatwebsite\Excel\Facades\Excel;

class CheckAndExportUsersProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $orders;

    protected $date;

    /**
     * Create a new job instance.
     *
     * @param Collection $orders
     * @param Carbon|null $date
     */
    public function __construct(Collection $orders, Carbon $date = null)
    {
        $this->orders = $orders;

        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->date) {
            $this->exportUserProductsList($this->date);
        } else {
            $this->exportUserProductsList(now()->addDays(2));
            /*
            Carbon::setLocale('en');

            switch (now()->format('l')) {
                case 'Wednesday':
                case 'Monday':
                    $this->exportUserProductsList(now()->addDays(3));
                    break;
                case 'Friday':
                    $this->exportUserProductsList(now()->addDays(4));
                    break;
            }*/
        }
    }

    public function exportUserProductsList(Carbon $date)
    {
        $this->orders = $this->orders->filter(function (Order $order) use ($date) {
            return $order->deliveries()->whereDate('date', $date->format('Y-m-d'))->exists();
        });

        $grouped = $this->orders->groupBy([
            function (Order $order) {
                return $order->user()->first()->getAttribute('name');
            },
        ], $preserveKeys = true);

        $grouped = $grouped->map(function (Collection $userGroup) use ($date) {
            $allProducts = collect();

            $userGroup->map(function (Order $order) use ($date, $allProducts) {
                /** @var Delivery $delivery */
                $delivery = $order->deliveries()->whereDate('date', $date->format('Y-m-d'))->first();

                $products = $order->products()->get();

                $products->map(function (Product $product) use ($allProducts, $order, $delivery, $date) {
                    $product->setAttribute('target_id', $product->pivot->target_id);

                    $quantity = Buyable::query()
                        ->where('order_id', $order->getKey())
                        ->where('buyable_type', 'Product')
                        ->where('buyable_id', $product->getKey())
                        ->first()
                        ->getAttribute('quantity');

                    for ($i = 0; $i < $quantity; $i++) {
                        $allProducts->push($product);
                    }
                });

                $mealPacks = $order->meal_packs()->get();

                $mealPacks->map(function (MealPack $mealPack) use ($allProducts, $delivery, $date, $order) {
                    $quantity = Buyable::query()
                        ->where('order_id', $order->getKey())
                        ->where('buyable_type', 'MealPack')
                        ->where('buyable_id', $mealPack->getKey())
                        ->first()
                        ->getAttribute('quantity');

                    $whichDay = $mealPack->products()->first()->pivot->day->value();

                    if ($whichDay == DayEnum::ONE_TIME) {
                        $products = $mealPack->products()->wherePivot('day', DayEnum::ONE_TIME)->get();
                    } else {
                        $products = collect();

                        $delivery->meal_pack_products()->get()->map(function (MealPackProduct $mealPackProduct) use ($products) {
                            $products->push($mealPackProduct->product()->first());
                        });
                    }

                    for ($i = 0; $i < $quantity; $i++) {
                        $products->map(function (Product $product) use ($allProducts, $mealPack) {
                            $product->setAttribute('target_id', $mealPack->getAttribute('target_id'));

                            $allProducts->push($product);
                        });
                    }
                });
            });

            $allProducts = $allProducts->groupBy([
                function (Product $product) {
                    return $product->getKey();
                },
            ], $preserveKeys = true);

            $allProducts = $allProducts->map(function (Collection $productGroup) {
                $product = $productGroup->first();

                $product->setAttribute('total', $productGroup->count());

                return $product;
            });

            return $allProducts->values();
        });

        $filename = "Clients Meals ({$date->format('l d.F')}).xlsx";

        $attachment = Excel::raw(new UsersProductsExport($grouped, $date), BaseExcel::XLSX);

//        Storage::disk('google')->put($filename, $attachment);

        $users = User::query()->role('Admin')->get();

        $users->map(function (User $user) use ($attachment, $filename, $date) {
            Mail::to($user)->send(new ProductsExport($attachment, $filename, $date));
        });
    }
}
