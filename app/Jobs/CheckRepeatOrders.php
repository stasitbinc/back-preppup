<?php

namespace App\Jobs;

use App\Models\Order;
use App\Models\User;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class CheckRepeatOrders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $users;

    /**
     * Create a new job instance.
     *
     * @param Collection $users
     */
    public function __construct(Collection $users)
    {
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @param OrderService $service
     * @return void
     */
    public function handle(OrderService $service)
    {
        $this->users->map(function (User $user) use ($service) {
            $orders = $user->orders()->get();

            $orders->map(function (Order $order) use ($service) {
                $date = Carbon::parse($order->getAttribute('delivery')->date);

                $doesntExist = Order::query()
                    ->where('delivery->date', $date->copy()->addWeek()->format('Y-m-d'))
                    ->doesntExist();

                if (! $date->copy()->subWeek()->diffInDays(now()) && $doesntExist) {
                    $service->duplicateOrder($order, $date->copy()->addWeek());
                }
            });
        });
    }
}
