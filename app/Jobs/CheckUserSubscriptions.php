<?php

namespace App\Jobs;

use App\Enums\DayEnum;
use App\Enums\OrderStatusEnum;
use App\Enums\SubscriptionStatusEnum;
use App\Models\MealPack;
use App\Models\Order;
use App\Models\Subscription;
use App\Models\User;
use App\Notifications\PaymentErrorNotification;
use App\Notifications\PaymentNotification;
use App\Notifications\User\UserPaymentErrorNotification;
use App\Notifications\User\UserPaymentNotification;
use App\Payment\Korta\Client;
use App\Services\OrderService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class CheckUserSubscriptions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $subscriptions;

    private $client;

    /**
     * Create a new job instance.
     *
     * @param Collection $subscriptions
     */
    public function __construct(Collection $subscriptions)
    {
        $this->subscriptions = $subscriptions;
    }

    /**
     * Execute the job.
     *
     * @param Client $client
     * @param OrderService $service
     * @return void
     */
    public function handle(Client $client, OrderService $service)
    {
        $this->subscriptions->map(function (Subscription $subscription) use ($client, $service) {
            /** @var Order $order */

            /** @var MealPack $mealPack */
            $order = $subscription->order()->first();

            $mealPack = $subscription->buyable()->first()->buyable()->first();

            $whichDay = $mealPack->products()->first()->pivot->day->value();

            $delivery = null;

            $subDays = 3;

            $delivery = $order->deliveries()->latest('date')->first();

            if ($whichDay == DayEnum::ONE_TIME) {
                $subDays = 2;
            }

            $date = $delivery->getAttribute('date');

            $dateWithSubDays = $date->copy()->subDays($subDays);

            if ($dateWithSubDays == now()) {
                $this->checkAndPay($subscription, $order, $mealPack, $client);

                $nextDate = $date->copy()->addWeek();

                if ($whichDay != DayEnum::ONE_TIME) {
                    if ($date->is(DayEnum::MONDAY)) {
                        $nextDate = $date->copy()->next(DayEnum::FRIDAY);
                    } else {
                        $nextDate = $date->copy()->next(DayEnum::MONDAY);
                    }
                }

                $service->createDeliveryDates($order, $nextDate, false);
            }
        });
    }

    public function checkAndPay(Subscription $subscription, Order $order, MealPack $mealPack, Client $client)
    {
        $dispatcher = Order::getEventDispatcher();

        Order::unsetEventDispatcher();

        if ($subscription->getAttribute('attempts_count') == 0) {
            $this->doPayment($client, $order, $mealPack, $subscription);
        } elseif ($subscription->getAttribute('attempts_count') < 2) {
            $this->doPayment($client, $order, $mealPack, $subscription);
        } else {
            $order->update([
                'status' => OrderStatusEnum::FAILED,
            ]);

            $subscription->update([
                'status' => SubscriptionStatusEnum::CANCELED,
            ]);
        }

        Order::setEventDispatcher($dispatcher);
    }

    public function doPayment(Client $client, Order $order, MealPack $mealPack, Subscription $subscription)
    {
        $response = $client->cofPayment($order, $mealPack->getAttribute('price'));

        if (Arr::get($response, 'd39') == 000) {
            $order->update([
                'status' => OrderStatusEnum::PAID,
            ]);

            $subscription->update([
                'attempts_count' => false,
            ]);

            $users = User::query()->role('Admin')->get();

            Notification::send($users, new PaymentNotification($response, $order));

            $user = $order->user()->first();

            $user->notify((new UserPaymentNotification($order))
                ->locale($user->language ? $user->language->language : 'is'));
        }

        if (Arr::get($response, 'd39') != 000) {
            $this->sendError($order, $subscription, $response);
        }
    }

    public function sendError(Order $order, Subscription $subscription, array $response)
    {
        $order->update([
            'status' => OrderStatusEnum::PENDING,
        ]);

        $subscription->update([
            'attempts_count' => DB::raw('attempts_count + 1'),
        ]);

        $users = User::query()->role('Admin')->get();

        Notification::send($users, new PaymentErrorNotification($response, $order));

        $user = $order->user()->first();

        $user->notify((new UserPaymentErrorNotification($order))->locale($user->language ? $user->language->language : 'is'));
    }
}
