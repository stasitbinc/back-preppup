<?php

namespace App\Payment\Pei;

use App\Models\Order;
use App\Models\User;
use App\Payment\Pei\Middleware\AccessToken;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Arr;
use JoeDixon\Translation\Language;
use Symfony\Component\HttpFoundation\Response;

class Client
{
    private $client;

    private $config;

    public function __construct(Guzzle $client, Config $config)
    {
        $this->client = $client;
        $this->config = $config;
    }

    public static function create(Config $config)
    {
        $stack = HandlerStack::create();

        $client = new Guzzle([
            'headers' => [
                'content-type' => 'application/json',
            ],
            'handler' => $stack,
            'timeout' => 30,
        ]);

        $stack->push(new AccessToken($client, $config));

        return new static($client, $config);
    }

    public function createOrderJson(Order $order): array
    {
        /** @var User $user */

        /** @var Language $language */
        $orderId = $order->getKey();

        $user = $order->user()->first();

        $language = $user->language()->first()->getAttribute('language');

        if ($language == 'is') {
            $language = '';
        }

        $buyables = $order->buyables()->get();

        $frontendUrl = config('app.frontend_url');

        $fields = [
            'merchantId' => $this->config->getMerchantId(),
            'amount' => 0,
            'successReturnUrl' => str_replace('//', '/', "$frontendUrl/$language/order/$orderId/pei"),
            'cancelReturnUrl' => str_replace('//', '/', "$frontendUrl/$language/order"),
            'postbackUrl' => str_replace('/', '/', "$frontendUrl/$language/order"),
            'reference' => $order->getAttribute('uuid'),
            'buyer' => [
                'name' => $user->getAttribute('name'),
                'email' => $user->getAttribute('email'),
                'mobileNumber' => $user->getAttribute('phone'),
            ],
        ];

        $total = 0;

        foreach ($buyables as $k => $buyable) {
            $amount = $buyable->buyable()->first()->getAttribute('price') * $buyable->getAttribute('quantity');

            $fields['items'][$k] = [
                'name' => $buyable->buyable()->first()->getAttribute('name'),
                'quantity' => $buyable->getAttribute('quantity'),
                'price' => $buyable->buyable()->first()->getAttribute('price'),
                'amount' => $amount,
                'unitPrice' => $buyable->buyable()->first()->getAttribute('price'),
            ];

            $fields['items'][$k]['code'] = $buyable['slug'];

            $total = $total + $amount;
        }

        if ($difference = $order->getAttribute('amount')->difference) {
            $total = $total + $difference;

            $fields['items'][] = [
                'name' => __('Pay the difference'),
                'quantity' => 1,
                'amount' => $difference,
                'unitPrice' => $difference,
            ];
        }

        if ($shipping = $order->getAttribute('amount')->shipping) {
            $total = $total + $shipping;

            $fields['items'][] = [
                'name' => __('Shipping'),
                'quantity' => 1,
                'amount' => $shipping,
                'unitPrice' => $shipping,
            ];
        }

        if ($coupon = $order->getAttribute('amount')->coupon) {
            $total = $total - $coupon;

            $fields['items'][] = [
                'name' => __('Coupon code'),
                'quantity' => 1,
                'amount' => -$coupon,
                'unitPrice' => -$coupon,
            ];
        }

        $fields['amount'] = $total;

        return $fields;
    }

    public function pay(Order $order): string
    {
        $jsonOrder = $this->createOrderJson($order);

        $orderId = $this->getOrderId($jsonOrder);

        $order->forceFill([
            'payment->pei->id' => $orderId,
        ])->save();

        return "{$this->config->getPaymentUrl()}{$orderId}";
    }

    public function getOrderId(array $jsonOrder): string
    {
        $response = $this->request($this->config->getOrderUrl(), ['form_params' => $jsonOrder], 'POST');

        abort_if(! Arr::get($response, 'orderId'), Response::HTTP_FORBIDDEN, __('Payment error'));

        return Arr::get($response, 'orderId');
    }

    public function getOrder(Order $order): array
    {
        $peiOrderId = $order->getAttribute('payment')->pei->id;

        return $this->request("{$this->config->getOrderUrl()}$peiOrderId");
    }

    protected function request($endpoint, $params = [], $method = 'GET')
    {
        $response = $this->client->request($method, $endpoint, $params);

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }
}
