<?php

namespace App\Payment\Pei\Middleware;

use App\Payment\Pei\BearerToken;
use App\Payment\Pei\Config;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Psr\Http\Message\RequestInterface;

class AccessToken
{
    private $token;

    private $client;

    private $config;

    public function __construct(Client $client, Config $config)
    {
        $this->client = $client;
        $this->config = $config;

        $this->token = Cache::get('peiAccessToken');
    }

    public function __invoke(callable $next)
    {
        return function (RequestInterface $request, array $options = []) use ($next) {
            $request = $this->applyToken($request);

            return $next($request, $options);
        };
    }

    protected function applyToken(RequestInterface $request)
    {
        if (! $this->hasValidToken()) {
            $this->acquireAccessToken();
        }

        return \GuzzleHttp\Psr7\modify_request($request, [
            'set_headers' => [
                'Authorization' => (string) $this->getToken(),
            ],
        ]);
    }

    private function acquireAccessToken()
    {
        $parameters = $this->getTokenRequestParameters();

        $credentials = base64_encode("{$this->config->getClientId()}:{$this->config->getSecretKey()}");

        $response = $this->client->request('POST', $this->config->getAuthUrl(), [
            'form_params' => $parameters,
            'handler' => \GuzzleHttp\choose_handler(),
            'headers' => [
                'authorization' => 'Basic '.$credentials,
            ],
        ]);

        $response = \GuzzleHttp\json_decode((string) $response->getBody(), true);

        if (! isset($response['access_token'])) {
            logger()->error('Invalid Access Token caught', compact('parameters', 'response') + ['uri' => $this->config->getAuthUrl()]);

            throw new \Exception(__('Invalid response [no access token]'));
        }

        $this->token = new BearerToken(
            $response['access_token'],
            (int) $response['expires_in']
        );

        Cache::remember('peiAccessToken', 3600, function () {
            return $this->token;
        });
    }

    private function getTokenRequestParameters()
    {
        return [
            'grant_type' => 'client_credentials',
            'scope' => 'externalapi',
        ];
    }

    public function getToken()
    {
        return $this->token;
    }

    private function hasValidToken()
    {
        return Cache::has('peiAccessToken');
    }
}
