<?php

namespace App\Payment\Netgiro;

use App\Http\Requests\PaymentRequest;
use App\Models\Order;
use Carbon\Carbon;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;

class Client
{
    /**
     * @var Guzzle
     */
    private $client;

    /**
     * @var Config
     */
    private $config;

    public function __construct(Guzzle $client, Config $config)
    {
        $this->client = $client;
        $this->config = $config;
    }

    /**
     * @param Config $config
     * @return Client
     */
    public static function create(Config $config)
    {
        $stack = HandlerStack::create();

        $client = new Guzzle([
            'base_uri' => $config->getUrl(),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Netgiro_AppKey' => $config->getAppId(),
                'Netgiro_Nonce' => $config->getNonce(),
            ],
            'handler' => $stack,
            'timeout' => 10000,
        ]);

        return new static($client, $config);
    }

    public function createPayment()
    {
        return $this->request('Create', ['form_params' => []], 'POST');
    }

    protected function request($endpoint, $params = [], $method = 'GET')
    {
        $headers = [
            'headers' => [
                'Netgiro_Signature' => hash('sha256', $this->config->getSecretKey().$this->config->getNonce().$this->config->getUrl().$endpoint),
            ],
        ];

        dump($this->config->getSecretKey().$this->config->getNonce().$this->config->getUrl().$endpoint);

        try {
            $response = $this->client->request($method, $endpoint, $params + $headers);
        } catch (GuzzleException $e) {
            dump($e);
        }

//        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }
}
