<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Allergy extends Model
{
    use HasTranslations;

    protected $fillable = [
        'title',
    ];

    public $translatable = [
        'title',
    ];

    protected $guarded = [
        'id',
        'created_at',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];
}
