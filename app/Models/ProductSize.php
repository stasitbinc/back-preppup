<?php

namespace App\Models;

use App\Models\Pivot\ProductProductSize;
use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model
{
    protected $fillable = [
        'kcal',
        'dish',
        'sauce',
        'product_id',
        'target_id',
    ];

    public function target()
    {
        return $this->belongsTo(Target::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_size')
            ->using(ProductProductSize::class);
    }
}
