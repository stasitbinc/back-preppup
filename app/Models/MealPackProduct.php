<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MealPackProduct extends Model
{
    protected $table = 'meal_pack_product';
    protected $fillable = [
        'id',
        'meal_pack_id',
        'product_id',
        'target_id',
        'week',
        'day',
        'count',
        'is_hidden',
        'sending',
        'diet',
    ];
}
