<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Postcode extends Model
{
    protected $fillable = [
        'zip',
        'standard',
        'extra',
        'free',
        'is_standard',
        'region_id',
        'is_limited',
    ];

    protected $casts = [
        'is_standard' => 'boolean',
        'is_limited' => 'boolean',
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }
}
