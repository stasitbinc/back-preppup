<?php

namespace App\Models;

use App\Models\Pivot\DeliveryMealPackProduct;
use App\Models\Pivot\MealPackProduct;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'order_id',
        'date',
    ];

    protected $dates = [
        'date',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function meal_pack_products()
    {
        return $this->belongsToMany(MealPackProduct::class, 'delivery_meal_pack_product', 'delivery_id', 'meal_pack_product_id')
            ->using(DeliveryMealPackProduct::class);
    }
}
