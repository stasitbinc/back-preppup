<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MealPackInfo extends Model
{
    protected $fillable = [
        'meal_pack_id',
        'id',
        'description',
    ];

    public function meal_pack()
    {
        return $this->belongsTo(MealPack::class);
    }
}
