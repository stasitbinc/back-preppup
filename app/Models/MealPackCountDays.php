<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class MealPackCountDays extends Model implements HasMedia
{
    use HasSlug, HasTranslations, HasMediaTrait;

    public $translatable = [
        'title',
        'description',
    ];

    protected $fillable = [
        'id',
        'title',
        'description',
        'slug',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'image',
    ];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function meal_packs()
    {
        return $this->hasMany(MealPack::class);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(366)
            ->keepOriginalImageFormat()
            ->height(366);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('pack')->singleFile();
    }

    public function getThumbAttribute()
    {
        if ($this->hasMedia('pack')) {
            return asset($this->getFirstMedia('pack')->getUrl('thumb'));
        }

        return asset('img/no-image.jpg');
    }

    public function getImageAttribute()
    {
        if ($this->hasMedia('pack')) {
            return asset($this->getFirstMediaUrl('pack'));
        }

        return asset('img/no-image.jpg');
    }
}
