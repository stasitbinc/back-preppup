<?php

namespace App\Models;

class StoreLocation extends User
{
    protected $table = 'users';

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
