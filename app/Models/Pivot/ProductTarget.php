<?php

namespace App\Models\Pivot;

use App\Models\Category;
use App\Models\Ingredient;
use App\Models\Product;
use App\Models\Target;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductTarget extends Pivot
{
    protected $table = 'product_target';

    public $incrementing = true;

    protected $fillable = [
        'product_id',
        'ingredient_id',
        'target_id',
        'category_id',
        'size',
    ];

    protected $casts = [
        'size' => 'float',
    ];

    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class);
    }

    public function target()
    {
        return $this->belongsTo(Target::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
