<?php

namespace App\Models\Pivot;

use App\Enums\DayEnum;
use App\Enums\WeekEnum;
use App\Models\MealPack;
use App\Models\Product;
use App\Models\Target;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Konekt\Enum\Eloquent\CastsEnums;

class MealPackProduct extends Pivot
{
    use CastsEnums;

    public $incrementing = true;

    protected $enums = [
        'week' => WeekEnum::class,
        'day' => DayEnum::class,
    ];

    protected $fillable = [
        'meal_pack_id',
        'product_id',
        'target_id',
        'week',
        'day',
        'count',
        'sending',
        'diet',
        'is_hidden',
    ];

    protected $casts = [
        'is_hidden' => 'boolean',
    ];

    public $timestamps = false;

    protected $attributes = [
        'week' => WeekEnum::__default,
        'day' => DayEnum::__default,
    ];

    public function meal_pack()
    {
        return $this->belongsTo(MealPack::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function target()
    {
        return $this->belongsTo(Target::class);
    }
}
