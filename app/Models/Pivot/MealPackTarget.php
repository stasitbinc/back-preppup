<?php

namespace App\Models\Pivot;

use App\Models\MealPack;
use App\Models\Target;
use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Relations\Pivot;

class MealPackTarget extends Pivot
{
    protected $table = 'meal_pack_target';

    protected $fillable = [
        'target_id',
        'meal_pack_id',
        'nutritional',
        'id',
    ];
    protected $attributes = [
        'nutritional' => '{}',
    ];

    public function target()
    {
        return $this->belongsTo(Target::class);
    }

    public function meal_pack()
    {
        return $this->belongsTo(MealPack::class);
    }
}
