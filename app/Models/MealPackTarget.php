<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MealPackTarget extends Model
{
    protected $table = 'meal_pack_target';
    protected $fillable = [
        'target_id',
        'id',
        'meal_pack_id',
        'nutritional',
        'created_at',
        'updated_at',
    ];

    public function meal_pack()
    {
        return $this->belongsTo(MealPack::class);
    }

    public function target()
    {
        return $this->belongsTo(Target::class);
    }
}
