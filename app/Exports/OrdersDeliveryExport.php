<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OrdersDeliveryExport implements WithMultipleSheets
{
    use Exportable;

    protected $orders;

    protected $date;

    public function __construct(Collection $orders, Carbon $date)
    {
        $this->orders = $orders;

        $this->date = $date;
    }

    public function sheets(): array
    {
        $sheets = [];

        $this->orders = $this->orders->groupBy('postcode.region.name');

        foreach ($this->orders as $region => $orders) {
            $sheets[] = new OrdersDeliveryPerRegionSheet($orders, $this->date, $region);
        }

        return $sheets;
    }
}
