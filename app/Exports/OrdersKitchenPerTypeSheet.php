<?php

namespace App\Exports;

use App\Models\ProductType;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class OrdersKitchenPerTypeSheet implements WithEvents, FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithTitle
{
    protected $meals;

    protected $date;

    protected $type;

    public function __construct(Collection $meals, Carbon $date, string $type)
    {
        $this->meals = $meals;

        $this->date = $date;

        $this->type = $type;
    }

    public function title(): string
    {
        return iconv('UTF-8', 'ASCII//TRANSLIT', str_replace('/', '-', $this->type));
    }

    public function collection()
    {
        return $this->meals;
    }

    public function headings(): array
    {
        return [
            $this->type.'/'.ProductType::query()
                ->where('title->is', $this->type)
                ->first()->getTranslation('title', 'en'),
            'Skera niður'.PHP_EOL.'Cut down',
            'Viðhalda'.PHP_EOL.'Maintenance',
            'Stækka'.PHP_EOL.'Bulk',
            'Samtals'.PHP_EOL.'Total',
        ];
    }

    public function map($meal): array
    {
        return [
            Arr::get($meal->toArray(), 'name'),
            Arr::get($meal->toArray(), 'cut_down'),
            Arr::get($meal->toArray(), 'maintenance'),
            Arr::get($meal->toArray(), 'bulk'),
            Arr::get($meal->toArray(), 'cut_down') + Arr::get($meal->toArray(), 'maintenance') + Arr::get($meal->toArray(), 'bulk'),
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:E1',
                    [
                        'font' => [
                            'size' => 15,
                            'color' => ['argb' => 'ffffff'],
                        ],
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'color' => ['argb' => '3b7b1e'],
                        ],
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_CENTER,
                            'vertical' => Alignment::VERTICAL_CENTER,
                        ],
                    ]
                );

                $rowNumber = $this->meals->count() + 2;

                $event->sheet->insertNewRowBefore($rowNumber, 1);

                $event->sheet->setCellValue("A$rowNumber", 'Samtals:');

                $bulk = 0;
                $cutDown = 0;
                $maintenance = 0;

                foreach ($this->meals as $meal) {
                    $bulk += Arr::get($meal->toArray(), 'bulk', 0);
                    $maintenance += Arr::get($meal->toArray(), 'maintenance', 0);
                    $cutDown += Arr::get($meal->toArray(), 'cut_down', 0);
                }

                $event->sheet->setCellValue("B$rowNumber", $cutDown);
                $event->sheet->setCellValue("C$rowNumber", $maintenance);
                $event->sheet->setCellValue("D$rowNumber", $bulk);
                $event->sheet->setCellValue("E$rowNumber", $cutDown + $bulk + $maintenance);

                $event->sheet->styleCells(
                    "A$rowNumber",
                    [
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_RIGHT,
                        ],
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'color' => ['argb' => 'dddddd'],
                        ],
                    ]
                );

                $event->sheet->styleCells(
                    "B$rowNumber:E$rowNumber",
                    [
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_RIGHT,
                        ],
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'color' => ['argb' => 'dddddd'],
                        ],
                    ]
                );

                $event->sheet->styleCells(
                    'B:E',
                    [
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_CENTER,
                        ],
                    ]
                );
            },
        ];
    }
}
