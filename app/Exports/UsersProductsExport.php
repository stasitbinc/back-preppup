<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class UsersProductsExport implements WithMultipleSheets
{
    use Exportable;

    protected $users;

    protected $date;

    public function __construct(Collection $users, Carbon $date)
    {
        $this->users = $users;

        $this->date = $date;
    }

    public function sheets(): array
    {
        $sheets = [];

        foreach ($this->users as $user => $products) {
            $sheets[] = new UsersProductsPerUserSheet($products, $this->date, $user);
        }

        return $sheets;
    }
}
