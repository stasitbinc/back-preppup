<?php

namespace App\Exports;

use App\Models\ProductType;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OrdersKitchenExport implements WithMultipleSheets
{
    use Exportable;

    protected $meals;

    protected $date;

    public function __construct(Collection $meals, Carbon $date)
    {
        $this->meals = $meals;

        $this->date = $date;
    }

    public function sheets(): array
    {
        $sheets = [];

        foreach ($this->meals as $type => $meals) {
            $productTypeWithTargets = ProductType::query()
                ->where('title->is', $type)
                ->first()
                ->getAttribute('with_targets');

            if ($productTypeWithTargets) {
                $sheets[] = new OrdersKitchenPerTypeSheet($meals, $this->date, $type);
            } else {
                $sheets[] = new OrdersKitchenListWithoutTargetsSheet($meals, $this->date, $type);
            }
        }

        return $sheets;
    }
}
