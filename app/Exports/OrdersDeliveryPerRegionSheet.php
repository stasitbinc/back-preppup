<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class OrdersDeliveryPerRegionSheet implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithEvents, WithTitle
{
    protected $orders;

    protected $date;

    protected $region;

    public function __construct(Collection $orders, Carbon $date, string $region)
    {
        $this->orders = $orders;

        $this->date = $date;

        $this->region = $region;
    }

    public function title(): string
    {
        return iconv('UTF-8', 'ASCII//TRANSLIT', str_replace('/', '-', $this->region));
    }

    public function collection()
    {
        return $this->orders;
    }

    public function headings(): array
    {
        return [
            'NAFN',
            'SÍMANÚMER',
            'HEIMILISFANG',
            'ÍBÚÐ/HÆÐ',
            'PNR',
            'HVERFI',
            'EF ÉG ER EKKI HEIMA?',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:G1',
                    [
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'color' => ['argb' => '31aa52'],
                        ],
                        'font' => [
                            'color' => ['argb' => 'ffffff'],
                        ],
                    ]
                );

                $event->sheet->styleCells(
                    'B:F',
                    [
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_CENTER,
                        ],
                    ]
                );

                $event->sheet->insertNewRowBefore(1, 1);

                $event->sheet->mergeCells('A1:F1');

                $event->sheet->setCellValue('A1', $this->region);

                $event->sheet->styleCells(
                    'A1:G1',
                    [
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_CENTER,
                        ],
                        'font' => [
                            'color' => ['argb' => '3b7b1e'],
                            'size' => 20,
                            'bold' => true,
                        ],
                    ]
                );

                Carbon::setlocale('is');

                $event->sheet->setCellValue('G1', $this->date->translatedFormat('l d.F'));
            },
        ];
    }

    public function map($order): array
    {
        return [
            $order->user->name,
            $order->phone,
            $order->address_value,
            $order->floor,
            $order->postcode ? $order->postcode->zip : '',
            $order->city,
            $order->delivery->if,
        ];
    }
}
