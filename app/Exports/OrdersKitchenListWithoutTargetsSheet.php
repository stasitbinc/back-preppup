<?php

namespace App\Exports;

use App\Models\ProductType;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class OrdersKitchenListWithoutTargetsSheet implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithTitle, WithEvents
{
    protected $meals;

    protected $date;

    protected $type;

    public function __construct(Collection $meals, Carbon $date, string $type)
    {
        $this->meals = $meals;

        $this->date = $date;

        $this->type = $type;
    }

    public function title(): string
    {
        return iconv('UTF-8', 'ASCII//TRANSLIT', str_replace('/', '-', $this->type));
    }

    public function collection()
    {
        return $this->meals;
    }

    public function headings(): array
    {
        return [
            $this->type.'/'.ProductType::query()
                ->where('title->is', $this->type)
                ->first()->getTranslation('title', 'en'),
            'Samtals'.PHP_EOL.'Total',
        ];
    }

    public function map($meal): array
    {
        return [
            Arr::get($meal->toArray(), 'name'),
            Arr::get($meal->toArray(), 'count'),
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:B1',
                    [
                        'font' => [
                            'size' => 15,
                            'color' => ['argb' => 'ffffff'],
                        ],
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'color' => ['argb' => '3b7b1e'],
                        ],
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_CENTER,
                            'vertical' => Alignment::VERTICAL_CENTER,
                        ],
                    ]
                );

                $rowNumber = $this->meals->count() + 2;

                $event->sheet->insertNewRowBefore($rowNumber, 1);

                $event->sheet->setCellValue("A$rowNumber", 'Samtals:');

                $event->sheet->setCellValue("B$rowNumber", $this->meals->count());

                $event->sheet->styleCells(
                    "A$rowNumber",
                    [
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_RIGHT,
                        ],
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'color' => ['argb' => 'dddddd'],
                        ],
                    ]
                );

                $event->sheet->styleCells(
                    "B$rowNumber",
                    [
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'color' => ['argb' => 'dddddd'],
                        ],
                    ]
                );
            },
        ];
    }
}
