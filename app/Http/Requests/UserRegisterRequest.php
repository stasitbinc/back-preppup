<?php

namespace App\Http\Requests;

use App\Enums\GenderEnum;
use App\Models\Postcode;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRegisterRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
            ],
            'age' => [
                'required',
                'numeric',
            ],
            'gender' => [
                'required',
                'string',
                Rule::in(GenderEnum::toArray()),
            ],
            'email' => [
                'required',
                'string',
                Rule::unique(app(User::class)->getTable(), 'email'),
            ],
            'phone' => [
                'required',
                'string',
            ],
            'address' => [
                'required',
                'string',
            ],
            'postcode_id' => [
                'required',
                'integer',
                Rule::exists(app(Postcode::class)->getTable(), 'id'),
            ],
            'floor' => [
                'required',
                'string',
            ],
            'city' => [
                'required',
                'string',
            ],
            'new_password' => [
                'required',
                'confirmed',
            ],
            'new_password_confirmation' => [
                'required',
            ],
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('Name'),
            'age' => __('Age'),
            'gender' => __('Sex'),
            'email' => __('email'),
            'city' => __('city'),
            'phone' => __('phone'),
            'address' => __('address'),
            'postcode_id' => __('postcode'),
            'floor' => __('floor'),
            'new_password' => __('Password'),
            'new_password_confirmation' => __('Confirm Password'),
        ];
    }
}
