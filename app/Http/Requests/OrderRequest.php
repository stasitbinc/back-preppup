<?php

namespace App\Http\Requests;

use App\Enums\DeliveryEnum;
use App\Enums\PaymentTypeEnum;
use App\Enums\ReservationOrderEnum;
use App\Models\Allergy;
use App\Models\Coupon;
use App\Models\Postcode;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderRequest extends FormRequest
{
    public function rules()
    {
        return [
            'step' => [
                'string',
                'required',
            ],
            'coupon_id' => [
                'integer',
                'nullable',
                Rule::exists(app(Coupon::class)->getTable(), 'id'),
            ],
            'overview.reservations' => [
                'exclude_unless:step,overview',
                'required',
                'string',
                Rule::in(ReservationOrderEnum::toArray()),
            ],
            'overview.terms' => [
                'exclude_unless:step,overview',
                'nullable',
                'boolean',
            ],
            'allergy.enabled' => [
                'exclude_unless:step,allergy',
                'required',
                'boolean',
            ],
            'allergy.intolerance' => [
                'exclude_unless:step,allergy',
                'nullable',
            ],
            'allergy.allergies' => [
                'exclude_unless:step,allergy',
                'nullable',
                'array',
            ],
            'allergy.allergies.*' => [
                'exclude_unless:step,allergy',
                'required_if:allergy.enabled,true',
                'integer',
                Rule::exists(app(Allergy::class)->getTable(), 'id'),
            ],
            'delivery.type' => [
                'exclude_unless:step,delivery',
                'required',
                'string',
                Rule::in(DeliveryEnum::toArray()),
            ],
            'delivery.date' => [
                'exclude_unless:step,delivery',
                'nullable',
                'required_if:delivery.type,'.DeliveryEnum::DAY,
                'date',
                'date_format:Y-m-d',
            ],
            'delivery.if' => [
                'exclude_unless:step,delivery',
                'required',
                'string',
            ],
            'buyables' => [
                'nullable',
                'exclude_unless:step,delivery',
                'required_if:delivery.type,'.DeliveryEnum::DAYS,
                'array',
                'size:'.$this->user('api')->cart->getAttribute('cart_items_count'),
            ],
            'buyables.*.date' => [
                'exclude_unless:step,delivery',
                'nullable',
                'required_if:delivery.type,'.DeliveryEnum::DAYS,
                'date',
                'date_format:Y-m-d',
            ],
            'address.is_same' => [
                'exclude_unless:step,address',
                'required_if:address.is_same,false',
                'boolean',
            ],
            'address.address' => [
                'exclude_unless:step,address',
                'required_if:address.is_same,false',
                'nullable',
                'string',
            ],
            'address.floor' => [
                'exclude_unless:step,address',
                'required_if:address.is_same,false',
                'nullable',
                'string',
            ],
            'address.postcode_id' => [
                'exclude_unless:step,address',
                'required_if:address.is_same,false',
                'nullable',
                'integer',
                Rule::exists(app(Postcode::class)->getTable(), 'id'),
            ],
            'address.city' => [
                'exclude_unless:step,address',
                'required_if:address.is_same,false',
                'nullable',
                'string',
            ],
            'address.phone' => [
                'exclude_unless:step,address',
                'required_if:address.is_same,false',
                'nullable',
                'string',
            ],
            'payment.type' => [
                'exclude_unless:step,payment',
                'required',
                'string',
                Rule::in(PaymentTypeEnum::toArray()),
            ],
        ];
    }

    public function attributes()
    {
        return [
            'allergy.intolerance' => __('intolerance'),
            'allergy.enabled' => __('allergy'),
            'allergy.allergies' => __('allergies'),
            'delivery.date' => __('delivery date'),
            'delivery.type' => __('delivery'),
            'delivery.if' => __("if I'm not home"),
            'buyables' => __('delivery dates'),
            'buyables.*.date' => __('delivery date'),
            'address.address' => __('address'),
            'address.floor' => __('floor'),
            'address.postcode_id' => __('postcode'),
            'address.city' => __('city'),
            'address.phone' => __('phone'),
            'address.is_same' => __('is the same'),
            'payment.type' => __('payment mode'),
        ];
    }
}
