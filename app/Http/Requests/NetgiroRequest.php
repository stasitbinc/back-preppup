<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NetgiroRequest extends FormRequest
{
    public function rules()
    {
        return [
            'transactionid' => [
                'required',
                'string',
            ],
            'referenceNumber' => [
                'required',
                'string',
            ],
            'invoiceNumber' => [
                'required',
                'string',
            ],
            'status' => [
                'required',
                'string',
            ],
            'accountNumber' => [
                'required',
                'string',
            ],
            'success' => [
                'required',
                'string',
            ],
            'paymentCode' => [
                'required',
                'string',
            ],
            'netgiroSignature' => [
                'required',
                'string',
            ],
            'totalAmount' => [
                'required',
                'string',
            ],
            'signature' => [
                'required',
                'string',
            ],
            'customerId' => [
                'nullable',
                'string',
            ],
        ];
    }
}
