<?php

namespace App\Http\Requests;

use App\Enums\ReservationOrderEnum;
use App\Models\Coupon;
use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CouponRequest extends FormRequest
{
    public function rules()
    {
        return [
            'code' => [
                'required',
                'string',
                Rule::exists(app(Coupon::class)->getTable(), 'code')->where('is_enabled', true),
                function ($attribute, $value, $fail) {
                    /** @var Order $order */
                    $order = Order::query()->findOrFail($this->get('order_id'));

                    $reservation = $order->getAttribute('overview')->reservations;

                    if ($reservation === ReservationOrderEnum::SUBSCRIBE) {
                        $fail(__('Discount :attribute cannot be used in subscription.', ['attribute' => $attribute]));
                    }

                    $coupon = Coupon::query()->where('code', $value)->first();

                    if ($coupon && $coupon->getAttribute('expiration_date') && now()->gt($coupon->getAttribute('expiration_date'))) {
                        $fail(__('Discount :attribute has been expired.', ['attribute' => $attribute]));
                    }

                    if ($coupon && $coupon->getAttribute('max_products') && $coupon->getAttribute('max_products') < $order->products()->count()) {
                        $fail(__('Your meal limit has expired, please contact our team to renew your contract.'));
                    }
                },
            ],
            'order_id' => [
                'required',
                'integer',
                Rule::exists(app(Order::class)->getTable(), 'id'),
            ],
        ];
    }

    public function attributes()
    {
        return [
            'code' => __('Discount code'),
        ];
    }
}
