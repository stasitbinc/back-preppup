<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactUsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
            ],
            'email' => [
                'required',
                'email',
            ],
            'message' => [
                'required',
                'string',
                'min:20',
            ],
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('Name'),
            'email' => __('email'),
            'message' => __('message'),
        ];
    }
}
