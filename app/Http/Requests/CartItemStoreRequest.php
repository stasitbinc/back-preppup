<?php

namespace App\Http\Requests;

use App\Models\Cart;
use App\Models\Target;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CartItemStoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            'id' => [
                'required',
                'integer',
            ],
            'type' => [
                'required',
                'string',
                Rule::in(['MealPack', 'Product']),
            ],
            'quantity' => [
                'required',
                'integer',
            ],
            'cart_id' => [
                'nullable',
                'integer',
                Rule::exists(app(Cart::class)->getTable(), 'id'),
            ],
            'target_id' => [
                'nullable',
                'integer',
                Rule::exists(app(Target::class)->getTable(), 'id'),
            ],
        ];
    }
}
