<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MealPack\MealPackCollection;
use App\Http\Resources\MealPack\MealPackResource;
use App\Http\Resources\MealPackCountDay\MealPackCountDayCollection;
use App\Http\Resources\MealPackGroup\MealPackGroupCollection;
use App\Http\Resources\MealPackGroup\MealPackGroupResource;
use App\Models\MealPack;
use App\Models\MealPackCountDays;
use App\Models\MealPackGroups;
use App\QueryFilters\MealPackFilters;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class MealPackController extends Controller
{
    public function index(Request $request)
    {
        $filters = MealPackFilters::hydrate($request->query());

        $mealPacks = MealPack::query()
            ->where('is_public', true)
            ->has('products')
            ->filterBy($filters)
            ->paginate($request->get('perPage'));

        return new MealPackCollection($mealPacks->load(
            'meal_pack_groups',
            'meal_pack_count_days',
            'products',
            'products.product_sizes',
            'products.targets',
            'meal_pack_infos'
        ));
    }

    public function show(MealPack $mealPack)
    {
        return new MealPackResource($mealPack->load(['products' => function ($query) {
            $query->wherePivot('is_hidden', false);
        }, 'products.targets',
            'products.product_type',
            'meal_pack_count_days',
            'meal_pack_infos',
            'products.product_sizes', ]));
    }

    public function showGroup(Request $request)
    {
        $mealPacks = QueryBuilder::for(MealPack::class)
            ->where('is_public', true)
            ->with(['meal_pack_groups' => function ($query) use ($request) {
                return $query
                    ->when(isset($request->get('filter')['meal_pack_groups.id']),
                        function ($query) use ($request) {
                            $query->where('id', $request->get('filter')['meal_pack_groups.id']);
                        });
            },
                'meal_pack_infos',
                'products',
                'products.product_sizes',
                'products.targets',
            ])
            ->allowedFilters([
                AllowedFilter::scope('meal_pack_groups.id', 'mealPackGroupsId'),
                AllowedFilter::scope('meal_pack_count_days.slug', 'mealPackCountDaysSlug'),
                'targets.slug', ])
            ->get();

        return new MealPackCollection($mealPacks);
    }

    public function group()
    {
        $mealPackGroup = MealPackGroups::query()->get();

        return new MealPackGroupCollection($mealPackGroup);
    }

    public function days()
    {
        $mealPackCount = MealPackCountDays::query()->get();

        return new MealPackCountDayCollection($mealPackCount);
    }

    public function search(Request $request)
    {
        $mealPacks = QueryBuilder::for(MealPack::class)
            ->where('is_public', true)
            ->with(['meal_pack_groups' => function ($query) use ($request) {
                return $query
                    ->when(isset($request->get('filter')['meal_pack_groups.id']),
                        function ($query) use ($request) {
                            $query->where('id', $request->get('filter')['meal_pack_groups.id']);
                        });
            },
                'meal_pack_infos',
                'products',
                'products.product_sizes',
                'products.targets',
            ])
            ->allowedFilters([
                AllowedFilter::scope('meal_pack_groups.id', 'mealPackGroupsId'),
                AllowedFilter::scope('meal_pack_count_days.slug', 'mealPackCountDaysSlug'),
                'targets.slug', ])
            ->paginate($request->get('per_page', 12));

        return new MealPackCollection($mealPacks);
    }
}
