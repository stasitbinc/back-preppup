<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Allergy\AllergyCollection;
use App\Models\Allergy;

class AllergiesController extends Controller
{
    public function index()
    {
        $allergies = Allergy::all();

        return new AllergyCollection($allergies);
    }
}
