<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CartItemStoreRequest;
use App\Http\Resources\Cart\CardResource;
use App\Http\Resources\CartItem\CardItemResource;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function show(Request $request, $cart = null)
    {
        /** @var User $user */
        $cart = Cart::query()->find($cart);

        $user = $request->user('api');

        if ($user && $cart) {
            if (is_null($user->cart()->first())) {
                $cart->update([
                    'user_id' => $user->getKey(),
                ]);
            } else {
                $cart = $user->cart()->first();
            }
        }

        if ($user && is_null($cart)) {
            $cart = $user->cart()->first();

            if (! $cart) {
                $cart = Cart::query()->create();
            }
        }

        if (is_null($user) && is_null($cart)) {
            $cart = Cart::query()->create();
        }

        return new CardResource($cart->load('cart_items.buyable'));
    }

    public function store(CartItemStoreRequest $request, $cart = null)
    {
        $cart = Cart::query()->firstOrCreate(['id' => $cart]);

        $id = $request->get('id');

        $type = $request->get('type');

        $quantity = $request->get('quantity');

        $targetId = $request->get('target_id');

        $item = app("\\App\\Models\\$type")->where('id', $id)->first();

        $cart->cart_items()->updateOrCreate([
            'buyable_type' => $type,
            'buyable_id' => $item->getKey(),
            'target_id' => $targetId,
        ], [
            'quantity' => DB::raw("quantity + $quantity"),
        ]);

        return (new CardResource($cart))
            ->additional([
                'message' => __('Product has been added to your cart.'),
            ])
            ->toResponse($request);
    }

    public function destroy(CartItem $cartItem, Request $request)
    {
        $cartItem->delete();

        return (new CardItemResource($cartItem))
            ->additional([
                'message' => __('Product has been removed from your cart.'),
            ])
            ->toResponse($request);
    }

    public function increase(CartItem $cartItem)
    {
        $cartItem->update([
            'quantity' => DB::raw('quantity + 1'),
        ]);

        return new CardItemResource($cartItem);
    }

    public function decrease(CartItem $cartItem)
    {
        $cartItem->update([
            'quantity' => DB::raw('quantity - 1'),
        ]);

        return new CardItemResource($cartItem);
    }
}
