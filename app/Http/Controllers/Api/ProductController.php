<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Product\ProductCollection;
use App\Models\Product;
use App\QueryFilters\ProductFilters;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $filters = ProductFilters::hydrate($request->query());

        $products = Product::query()
            ->has('product_type')
            ->with('product_type')
            ->where('is_hidden', false)
            ->filterBy($filters)
            ->paginate($request->get('perPage'));

        return new ProductCollection($products);
    }

    public function testProduct()
    {
        $products = Product::query()
            ->has('product_type')
            ->with('product_type')
            ->where('is_hidden', false)
            ->get();

        return new ProductCollection($products);
    }
}
