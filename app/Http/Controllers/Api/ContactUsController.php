<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactUsRequest;
use App\Models\User;
use App\Notifications\ContactUsNotification;
use App\Notifications\User\UserContactUsNotification;
use Illuminate\Support\Facades\Notification;

class ContactUsController extends Controller
{
    public function store(ContactUsRequest $request)
    {
        $users = User::query()->role('Admin')->get();

        Notification::send($users, new ContactUsNotification($request->validated()));

        Notification::route('mail', $request->get('email'))
            ->notify((new UserContactUsNotification($request->get('name')))->locale(config('app.locale')));

        return response()->json(['message' => __("Thank you for contacting us. We'll be in touch soon.")]);
    }
}
