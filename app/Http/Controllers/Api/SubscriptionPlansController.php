<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SubscriptionPlan\SubscriptionPlanCollection;
use App\Models\SubscriptionPlan;

class SubscriptionPlansController extends Controller
{
    public function index()
    {
        $types = SubscriptionPlan::all();

        return new SubscriptionPlanCollection($types);
    }
}
