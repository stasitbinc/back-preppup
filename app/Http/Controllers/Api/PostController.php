<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Post\PostCollection;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $mealPacks = Post::query()
            ->with('target')
            ->paginate($request->get('perPage'));

        return new PostCollection($mealPacks);
    }
}
