<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Target\TargetCollection;
use App\Models\Target;

class TargetController extends Controller
{
    public function index()
    {
        $targets = Target::all();

        return new TargetCollection($targets);
    }
}
