<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Term\TermCollection;
use App\Models\Term;

class TermsController extends Controller
{
    public function index()
    {
        $terms = Term::all();

        return new TermCollection($terms);
    }
}
