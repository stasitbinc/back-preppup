<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\Buyable\BuyableCollection;
use App\Http\Resources\Cart\CardResource;
use App\Http\Resources\Coupon\CouponResource;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'user' => new UserResource($this->whenLoaded('user')),
            'coupon' => new CouponResource($this->whenLoaded('coupon')),
            'cart' => new CardResource($this->whenLoaded('cart')),
            'buyables' => new BuyableCollection($this->whenLoaded('buyables')),
        ]);
    }
}
