<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\MealPack\MealPackCollection;
use App\Http\Resources\ProductSize\ProductSizeResource;
use App\Http\Resources\ProductType\ProductTypeResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request) :array
    {

//        return array_merge(parent::toArray($request), [
//            'meal_packs' => new MealPackCollection($this->whenLoaded('meal_packs')),
//            'week' => $this->whenPivotLoaded('meal_pack_product', function () {
//                return $this->pivot->week->value();
//            }),
//            'day' => $this->whenPivotLoaded('meal_pack_product', function () {
//                return $this->pivot->day->value();
//            }),
//            'target' => $this->whenPivotLoaded('meal_pack_product', function () {
//                return $this->pivot->target;
//            }),
//            'count' => $this->whenPivotLoaded('meal_pack_count_days', function () {
//                return $this->pivot->count;
//            }),
//        ]);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'ingredients' => $this->ingredients,
            'allergy' => $this->allergy,
            'price' => $this->price,
            'image' => $this->getImageAttribute(),
            'images' => $this->getImagesAttribute(),
            'is_additional' => $this->is_additional,
            'is_hidden' => $this->is_hidden,
            'created_at' => $this->created_at,
            'week' => $this->whenPivotLoaded('meal_pack_product', function () {
                return $this->pivot->week->value();
            }),
            'day' => $this->whenPivotLoaded('meal_pack_product', function () {
                return $this->pivot->day->value();
            }),
            'count' => $this->whenPivotLoaded('meal_pack_product', function () {
                return $this->pivot->count;
            }),
            'sending' => $this->whenPivotLoaded('meal_pack_product', function () {
                return (array) explode(',', $this->pivot->sending);
            }),
            'diet' => $this->whenPivotLoaded('meal_pack_product', function () {
                return $this->pivot->diet;
            }),
            'product_type' => new ProductTypeResource($this->product_type),
            'product_sizes' => ProductSizeResource::collection($this->whenLoaded('product_sizes')),
        ];
    }
}
