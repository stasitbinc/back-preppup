<?php

namespace App\Http\Resources\Faq;

use App\Http\Resources\FaqCategory\FaqCategoryResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FaqResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'faq_category' => new FaqCategoryResource($this->whenLoaded('faq_category')),
        ]);
    }
}
