<?php

namespace App\Http\Resources\MealPackCountDay;

use App\Http\Resources\MealPack\MealPackCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class MealPackCountDayResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) :array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'slug' => $this->slug,
            'image' => $this->getImageAttribute(),
        ];
    }
}
