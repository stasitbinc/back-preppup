<?php

namespace App\Http\Resources\Buyable;

use App\Http\Resources\MealPack\MealPackResource;
use App\Http\Resources\Order\OrderResource;
use App\Http\Resources\Product\ProductResource;
use App\Models\MealPack;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BuyableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'order' => new OrderResource($this->whenLoaded('order')),
            'buyable' => $this->when($this->whenLoaded('buyable'), function () {
                if ($this->resource->buyable instanceof MealPack) {
                    return new MealPackResource($this->resource->buyable);
                }

                if ($this->resource->buyable instanceof Product) {
                    return new ProductResource($this->resource->buyable);
                }
            }),
        ]);
    }
}
