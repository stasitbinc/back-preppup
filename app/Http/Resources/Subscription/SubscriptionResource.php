<?php

namespace App\Http\Resources\Subscription;

use App\Http\Resources\Buyable\BuyableResource;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'user' => new UserResource($this->whenLoaded('user')),
            'buyable' => new BuyableResource($this->whenLoaded('buyable')),
        ]);
    }
}
