<?php

namespace App\Http\Resources\MealPackGroup;

use App\Http\Resources\MealPack\MealPackCollection;
use App\Http\Resources\MealPackCountDay\MealPackCountDayResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MealPackGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'sub_title' => $this->sub_title,
            'description' => $this->description,
            'slug' => $this->slug,
            'created_at' => $this->created_at,
            'image' => $this->getImageAttribute(),
//            'meal_packs' => new MealPackCollection($this->whenLoaded('meal_packs')),
            'meal_packs' => MealPackCollection::collection($this->whenLoaded('meal_packs')),
        ];
    }
}
