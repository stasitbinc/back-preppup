<?php

namespace App\Http\Resources\MealPackInfo;

use Illuminate\Http\Resources\Json\JsonResource;

class MealPackInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) :array
    {
        return [
          'info' =>  json_decode($this->description),
        ];
    }
}
