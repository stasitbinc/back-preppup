<?php

namespace App\Http\Resources\Coupon;

use App\Http\Resources\Order\OrderCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CouponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'orders' => new OrderCollection($this->whenLoaded('orders')),
        ]);
    }
}
