<?php

namespace App\Http\Resources\MealPackTarget;

use App\Http\Resources\MealPack\MealPackCollection;
use App\Http\Resources\Post\PostCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MealPackTargetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return  [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'nutritional' => json_decode($this->whenPivotLoaded('meal_pack_target',
                function () {
                    return $this->pivot->nutritional;
                }), true),
        ];
    }
}
