<?php

namespace App\Http\Resources\Target;

use App\Http\Resources\MealPack\MealPackCollection;
use App\Http\Resources\Post\PostCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TargetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return  [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'posts' => new PostCollection($this->whenLoaded('posts')),
        ];
    }
}
