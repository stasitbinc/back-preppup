<?php

namespace App\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Filter records based on query parameters.
 */
class ProductFilters extends QueryFilters
{
    public function isAdditional()
    {
        $this->query->where('is_additional', true);
    }

    public function random()
    {
        $this->query->orderByRaw('RAND()');
    }

    public function type($type)
    {
        $this->query->whereHas('product_type', function (Builder $query) use ($type) {
            $query->where('slug', $type);
        });
    }

    public function sort($sort)
    {
        switch ($sort) {
            case 'kcal':
                switch ($this->request->get('direction')) {
                    case 'desc':
                        $this->query->join('product_size', 'product_size.product_id', '=', 'products.id')
                            ->join('product_sizes', 'product_size.product_size_id', '=', 'product_sizes.id')
                            ->select('products.*', DB::raw('MAX(product_sizes.kcal) as kcal'))
                            ->groupBy('product_id')
                            ->orderBy('kcal', 'DESC');
                        break;
                    default:
                        $this->query->join('product_size', 'product_size.product_id', '=', 'products.id')
                            ->join('product_sizes', 'product_size.product_size_id', '=', 'product_sizes.id')
                            ->select('products.*', DB::raw('MIN(product_sizes.kcal) as kcal'))
                            ->groupBy('product_id')
                            ->orderBy('kcal', 'ASC');
                }
                break;
            default:
                switch ($this->request->get('direction')) {
                    case 'desc':
                        $this->query->orderByDesc($sort);
                        break;
                    default:
                        $this->query->orderBy($sort);
                }
        }
    }
}
