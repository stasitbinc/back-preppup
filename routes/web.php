<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('admin');
});

Route::get('/test', function () {
    $order = \App\Models\Order::query()->find(1927);

    $products = $order->products()->limit(1)->get();

    $products = $products->map(function (App\Models\Product $product) {
        return $product->setAttribute('target_id', $product->pivot->target_id);
    });

//    return view('stickers', compact('products'));
    return \PDF::loadView('stickers', compact('products'))->setPaper('a5')->stream();
});

Route::get('order/{order}.pdf', 'Api\\OrderController@exportPdf')->name('download');
