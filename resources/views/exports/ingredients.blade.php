@foreach($meals as $category => $targets)
    <table>
        <thead>
        <tr>
            <th style="background-color: #1e7adb; color: #ffffff; text-align: center; font-size: 12px; font-weight: bold; font-family: Arial, Helvetica, sans-serif; vertical-align: center;">
                {{iconv('UTF-8', 'ASCII//TRANSLIT', str_replace("/", '-', $category))}}
            </th>
            <th style="background-color: #1e7adb; color: #ffffff; text-align: center; font-size: 12px; font-weight: bold; font-family: Arial, Helvetica, sans-serif; vertical-align: center;">
                Total
            </th>
            <th style="background-color: #1e7adb; color: #ffffff; text-align: center; font-size: 12px; font-weight: bold; font-family: Arial, Helvetica, sans-serif; vertical-align: center;">
                Samtals <br> Total
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($targets as $target)
            <tr>
                <td style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; text-align: left;">{{iconv('UTF-8', 'ASCII//TRANSLIT', $target['name'])}}</td>
                <td style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; text-align: center;">{{$target['count']}} stk</td>
                <td style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; text-align: center;">{{$target['total']}} gr</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endforeach