@component('mail::message')
# Hello!

You can download Ingredients List Export file from the Attachments.

Best regards,<br>
{{ config('app.name') }}
@endcomponent
