@component('mail::message')
# Hello!

You can download Delivery Export file from the Attachments.

Best regards,<br>
{{ config('app.name') }}
@endcomponent
