@component('mail::message')
# Hello!

You can download Kitchen List Export file from the Attachments.

Best regards,<br>
{{ config('app.name') }}
@endcomponent
