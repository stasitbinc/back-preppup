<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Lykilorðið hefur verið endurstillt !',
    'sent' => 'Nýtt lykilorð hefur verið sent á netfangið þitt!',
    'throttled' => 'Hinkraðu augnabli áður en þú reynir aftur.',
    'token' => 'Þessi hlekkur er ekki gildur.',
    'user' => 'Enginn notandi finnst með þessu netfangi.',

];
