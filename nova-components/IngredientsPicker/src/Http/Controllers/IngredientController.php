<?php

namespace Preppup\IngredientsPicker\Http\Controllers;

use App\Models\Category;
use App\Models\Ingredient;
use App\Models\Product;
use App\Models\Target;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Pivot\ProductTarget;

class IngredientController extends Controller
{
    public function show(Product $product)
    {
        return $product;
    }

    public function index(Request $request)
    {
        $targets = Target::query()
            ->has('categories.ingredients')
            ->get();

        $targets = $targets->map(function (Target $target) use ($request) {
            $target->categories = $target->categories()->get()->map(function (Category $category) use ($request, $target) {
                $category->ingredients = $category->ingredients()->get()->map(function (Ingredient $ingredient) use ($request, $category, $target) {
                    $productTarget = ProductTarget::query()
                        ->where('product_id', $request->get('product_id'))
                        ->where('target_id', $target->getKey())
                        ->where('ingredient_id', $ingredient->getKey())
                        ->where('category_id', $category->getKey())
                        ->first();

                    if ($productTarget) {
                        $ingredient->setAttribute('size', $productTarget->getAttribute('size'));
                        $ingredient->setAttribute('is_enabled', true);
                    }

                    return $ingredient->toArray();
                });

                return $category;
            });

            return $target;
        });

        return $targets;
    }
}