<?php

namespace Preppup\IngredientsPicker;

use Illuminate\Support\Facades\Route;
use Laravel\Nova\Http\Middleware\Authorize;
use Laravel\Nova\Nova;
use Laravel\Nova\Events\ServingNova;
use Illuminate\Support\ServiceProvider;

class FieldServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->booted(function () {
            $this->routes();
        });

        Nova::serving(function (ServingNova $event) {
            Nova::script('ingredients-picker', __DIR__.'/../dist/js/field.js');
            Nova::style('ingredients-picker', __DIR__.'/../dist/css/field.css');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function routes()
    {
        if ($this->app->routesAreCached()) {
            return;
        }

        Route::middleware(['nova', Authorize::class])
            ->prefix('nova-vendor/ingredients')
            ->group(__DIR__.'/../routes/api.php');
    }
}
