Nova.booting((Vue, router, store) => {
  Vue.component('detail-ingredients-picker', require('./components/DetailField'))
  Vue.component('form-ingredients-picker', require('./components/FormField'))
})
