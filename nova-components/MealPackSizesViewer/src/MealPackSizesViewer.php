<?php

namespace Preppup\MealPackSizesViewer;

use Laravel\Nova\Fields\Field;

class MealPackSizesViewer extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'meal-pack-sizes-viewer';

    public $showOnIndex = false;

    public $showOnCreation = false;

    public $showOnUpdate = false;
}
