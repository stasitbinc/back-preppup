<?php

namespace Preppup\MealPackInfo;

use Laravel\Nova\Fields\Field;

class MealPackInfo extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'meal-pack-info';
}
