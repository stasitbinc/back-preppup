<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropMealPackTarget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_packs', function (Blueprint $table) {
            $table->dropForeign('meal_packs_target_id_foreign');
            $table->dropColumn('target_id');
            $table->dropColumn('nutritional');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_packs', function (Blueprint $table) {
            $table->json('nutritional');
            $table->foreign('target_id')->references('id')->on('targets')->onDelete('cascade');
            $table->unsignedBigInteger('target_id')->nullable()->after('id');
        });
    }
}
