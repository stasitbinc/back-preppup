<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnMealPackCountDaysIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_packs', function (Blueprint $table) {
            $table->unsignedBigInteger('meal_pack_count_days_id')->nullable()->after('id');

            $table->foreign('meal_pack_count_days_id')->references('id')->on('meal_pack_count_days')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_packs', function (Blueprint $table) {
            $table->dropForeign('meal_packs_meal_pack_count_days_id_foreign');
            $table->dropColumn(['meal_pack_count_days_id']);
        });
    }
}
