<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnCountDaysMealPacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('count_days_meal_packs', function (Blueprint $table) {
            $table->renameColumn('meal_packs_id', 'meal_pack_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('count_days_meal_packs', function (Blueprint $table) {
            $table->renameColumn('meal_pack_id', 'meal_packs_id');
        });
    }
}
