<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropProductSizeColimnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_size', function (Blueprint $table) {
            $table->dropForeign('product_size_product_size_id_foreign');
            $table->dropColumn('product_size_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_size', function (Blueprint $table) {
            $table->foreign('product_size_id')->references('id')->on('product_sizes')->onDelete('cascade');
            $table->unsignedBigInteger('product_size_id')->nullable()->after('id');
        });
    }
}
