<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropProductSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_pack_product', function (Blueprint $table) {
            $table->dropForeign('meal_pack_product_product_size_id_foreign');
            $table->dropColumn('product_size_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_pack_product', function (Blueprint $table) {
            $table->foreign('product_size_id')->references('id')->on('product_sizes')->onDelete('cascade');
            $table->unsignedBigInteger('product_size_id')->nullable()->after('id');
        });
    }
}
