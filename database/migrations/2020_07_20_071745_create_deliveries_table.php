<?php

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');

            $table->date('date');
        });

        $orders = Order::all();

        $orders->map(function (Order $order) {
            $deliveryDate = Carbon::parse($order->getAttribute('delivery')->date);

            $product = $order->products()->first();

            $mealPacks = $order->meal_packs()->get();

            if ($mealPacks->count()) {
                for ($i = 0; $i < $mealPacks->max('weeks'); $i++) {
                    $order->deliveries()->create([
                       'date' => $deliveryDate->copy()->addWeeks($i)->format('Y-m-d'),
                    ]);
                }
            } elseif ($product && $deliveryDate) {
                $order->deliveries()->create([
                    'date' => $deliveryDate->format('Y-m-d'),
                ]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
