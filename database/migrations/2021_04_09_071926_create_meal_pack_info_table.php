<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealPackInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_pack_infos', function (Blueprint $table) {
            $table->unsignedBigInteger('meal_pack_id')->nullable();
            $table->foreign('meal_pack_id')->references('id')->on('meal_packs')->onDelete('cascade');
            $table->json('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_pack_infos');
    }
}
