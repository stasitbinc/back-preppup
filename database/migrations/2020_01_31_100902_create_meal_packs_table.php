<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealPacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_packs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('meal_pack_type_id');
            $table->json('name');
            $table->string('slug');
            $table->json('sub_title');
            $table->json('ingredients')->nullable();
            $table->json('allergy')->nullable();
            $table->json('description')->nullable();
            $table->json('nutritional');
            $table->unsignedBigInteger('price');
            $table->unsignedBigInteger('price_meal');
            $table->unsignedBigInteger('kcal')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('meal_pack_type_id')->references('id')->on('meal_pack_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_packs');
    }
}
