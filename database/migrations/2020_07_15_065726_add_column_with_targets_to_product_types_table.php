<?php

use App\Models\ProductType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnWithTargetsToProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->boolean('with_targets')->default(false)->after('slug');
        });

        ProductType::all()->map(function (ProductType $productType) {
            switch ($productType->getAttribute('slug')) {
                case 'meat-1':
                case 'fish-1':
                case 'breakfast':
                    $with_targets = true;
                    break;
                default:
                    $with_targets = false;
                    break;
            }

            $productType->update(compact('with_targets'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_types', function (Blueprint $table) {
            $table->dropColumn(['with_targets']);
        });
    }
}
