<?php

use App\Models\MealPack;
use App\Models\Target;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AttachTargetsToMealPacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_packs', function (Blueprint $table) {
            $table->unsignedBigInteger('target_id')->nullable()->after('id');

            $table->foreign('target_id')->references('id')->on('targets')->onDelete('cascade');
        });

        if (DB::table('meal_pack_types')->select(['title'])->where('slug', 'low-carb')->exists()) {
            $lowCarb = json_decode(DB::table('meal_pack_types')->select(['title'])->where('slug', 'low-carb')->first()->title);

            Target::query()->create([
                'name' => [
                    'en' => $lowCarb->en,
                    'is' => $lowCarb->is,
                ],
            ]);
        }

        if (DB::table('meal_pack_types')->select(['title'])->where('slug', 'vegan')->exists()) {
            $vegan = json_decode(DB::table('meal_pack_types')->select(['title'])->where('slug', 'vegan')->first()->title);

            Target::query()->create([
                'name' => [
                    'en' => $vegan->en,
                    'is' => $vegan->is,
                ],
            ]);
        }

        $mealPacks = MealPack::withTrashed()->get();

        $mealPacks->map(function (MealPack $mealPack) {
            $type = json_decode(DB::table('meal_pack_types')->select(['title'])->where('id', $mealPack->getAttribute('meal_pack_type_id'))->first()->title);

            $mealPack->update([
                'target_id' => Target::query()->where('name->en', $type->en)->first()->getKey(),
            ]);
        });

        Schema::table('meal_packs', function (Blueprint $table) {
            $table->dropForeign('meal_packs_meal_pack_type_id_foreign');

            $table->dropColumn(['meal_pack_type_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_packs', function (Blueprint $table) {
            $table->dropForeign('meal_packs_target_id_foreign');

            $table->dropColumn(['target_id']);

            $table->unsignedBigInteger('meal_pack_type_id')->after('id');

            $table->foreign('meal_pack_type_id')->references('id')->on('meal_pack_types')->onDelete('cascade');
        });
    }
}
