<?php

use Illuminate\Database\Migrations\Migration;
use JoeDixon\Translation\Language;

class UpdateLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $languages = Language::all();

        $languages->map(function (Language $language) {
            if ($language->getAttribute('language') == 'en') {
                $language->update([
                    'name' => __('English'),
                ]);
            }
            if ($language->getAttribute('language') == 'is') {
                $language->update([
                    'name' => __('Ísland'),
                ]);
            }
        });

        Language::query()->create([
            'name' => __('Ísland'),
            'language' => 'is',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
