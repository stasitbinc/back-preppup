<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropTableCountDayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('count_days_meal_packs');
        Schema::drop('count_days');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('count_days', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('title')->nullable();
            $table->json('description')->nullable();
            $table->string('slug')->nullable();
            $table->timestamps();
        });
        Schema::create('count_days_meal_packs', function (Blueprint $table) {
            $table->unsignedBigInteger('meal_packs_id')->nullable();
            $table->unsignedBigInteger('count_days_id')->nullable();

            $table->foreign('meal_packs_id')->references('id')->on('meal_packs')->onDelete('cascade');
            $table->foreign('count_days_id')->references('id')->on('count_days')->onDelete('cascade');
        });
    }
}
