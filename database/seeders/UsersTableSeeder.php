<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Treestoneit\ShoppingCart\Facades\Cart;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User $user */
        $user = User::factory()->create([
            'email' => 'test@test.com',
        ]);

        $user->assignRole('Admin');

        $user = User::factory()->create([
            'email' => 'user@user.com',
        ]);

        $user->assignRole('User');
    }
}
