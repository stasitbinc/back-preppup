<?php

namespace Database\Seeders;

use Database\Seeders\AllergiesTableSeeder;
use Database\Seeders\CategoriesTableSeeder;
use Database\Seeders\CouponsTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PostcodesTableSeeder::class);
        $this->call(TermsTableSeeder::class);
        $this->call(CouponsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(FaqCategoriesTableSeeder::class);
        $this->call(AllergiesTableSeeder::class);
        $this->call(SubscriptionPlansTableSeeder::class);
        $this->call(ProductTypesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TargetsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
//        $this->call(ProductSizesTableSeeder::class);
        $this->call(PostTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(IngredientsTableSeeder::class);
        $this->call(OauthClientsTableSeeder::class);
        $this->call(MealPackCountDaySeeder::class);
        $this->call(MealPackGroupsSeeder::class);
        $this->call(MealPackSeeder::class);
        $this->call(MealPackProductSeeder::class);
        $this->call(MealPackTargetSeeder::class);
        $this->call(MealPackInfoSeeder::class);
    }
}
//
//php artisan db:seed --class=MealPackCountDaySeeder,
//php artisan db:seed --class=MealPackGroupsSeeder
//php artisan db:seed --class=MealPackSeeder
//php artisan db:seed --class=MealPackProductSeeder
//php artisan db:seed --class=MealPackTargetSeeder
//php artisan db:seed --class=MealPackInfoSeeder
