<?php

namespace Database\Seeders;

use App\Models\MealPack;
use Illuminate\Database\Seeder;

class MealPacksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MealPack::factory()->count(20)->create();
    }
}
