<?php

namespace Database\Seeders;

use App\Models\SubscriptionPlan;
use Illuminate\Database\Seeder;

class SubscriptionPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubscriptionPlan::factory()->create([
            'days' => 5,
            'hours_to_save' => 7,
        ]);

        SubscriptionPlan::factory()->create([
            'days' => 7,
            'hours_to_save' => 9,
        ]);

        SubscriptionPlan::factory()->create([
            'days' => 28,
            'hours_to_save' => 37,
        ]);

        SubscriptionPlan::factory()->create([
            'days' => 56,
            'hours_to_save' => 70,
        ]);
    }
}
