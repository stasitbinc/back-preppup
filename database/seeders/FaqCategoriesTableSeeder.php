<?php

namespace Database\Seeders;

use App\Models\FaqCategory;
use Database\Seeders\CategoriesTableSeeder;
use Illuminate\Database\Seeder;

class FaqCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FaqCategory::factory()->count(6)->create();
    }
}
