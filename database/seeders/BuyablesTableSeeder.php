<?php

namespace Database\Seeders;

use App\Models\Buyable;
use Illuminate\Database\Seeder;

class BuyablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Buyable::factory()->count(30)->create();
    }
}
