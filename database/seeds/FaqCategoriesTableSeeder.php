<?php

use Illuminate\Database\Seeder;
use App\Models\FaqCategory;

class FaqCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(FaqCategory::class, 6)->create();
    }
}
