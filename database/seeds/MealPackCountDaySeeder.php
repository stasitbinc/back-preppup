<?php

use Illuminate\Database\Seeder;
use  App\Models\MealPackCountDays;

class MealPackCountDaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MealPackCountDays::query()->create([
            'title' => ['en' => "5", 'is' => "IS 5"],
            'description' => '',
            'slug' => 'five-day',
        ]);

        MealPackCountDays::query()->create([
            'title' => ['en' => "7", 'is' => "7"],
            'description' => '',
            'slug' => 'seven-day',
        ]);

        MealPackCountDays::query()->create([
            'title' => ['en' => "28", 'is' => "28"],
            'description' => '',
            'slug' => 'twenty-eathe',
        ]);

        MealPackCountDays::query()->create([
            'title' => ['en' => "Subscription", 'is' => "Áskrift"],
            'description' => '',
            'slug' => 'subscription',
        ]);
    }
}
