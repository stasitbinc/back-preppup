<?php

use App\Models\MealPack;
use Illuminate\Database\Seeder;

class MealPacksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(MealPack::class, 20)->create();
    }
}
