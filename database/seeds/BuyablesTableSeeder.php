<?php

use App\Models\Buyable;
use Illuminate\Database\Seeder;

class BuyablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Buyable::class, 30)->create();
    }
}
