<?php

namespace Database\Factories;

use App\Models\Buyable;
use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class BuyableFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Buyable::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'order_id' => Order::query()->inRandomOrder()->first(),
            'quantity' => $this->faker->randomDigitNotNull,
            'buyable_type' => $this->faker->randomElement(['Product', 'MealPack']),
            'buyable_id' => $this->faker->numberBetween(1, 20),
            'kcal' => $this->faker->randomNumber(4),
            'price' => $this->faker->randomNumber(4),
        ];
    }
}
