<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Ingredient;
use Illuminate\Database\Eloquent\Factories\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class IngredientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ingredient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $this->faker->addProvider(new \FakerRestaurant\Provider\en_US\Restaurant($faker));

        return [
            'name' => $this->faker->foodName(),
            'calories' => $this->faker->randomNumber(2),
            'fat' => $this->faker->randomFloat(1, 0, 10),
            'protein' => $this->faker->randomFloat(1, 0, 10),
            'carbohydrates' => $this->faker->randomFloat(1, 0, 10),
            'category_id' => Category::query()->inRandomOrder()->first(),
        ];
    }
}
