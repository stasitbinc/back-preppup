<?php

namespace Database\Factories;

use App\Enums\DayEnum;
use App\Enums\WeekEnum;
use App\Models\MealPack;
use App\Models\Product;
use App\Models\Target;
use Illuminate\Database\Eloquent\Factories\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class MealPackFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MealPack::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (MealPack $mealPack, Faker $faker) {
            $products = Product::all()->random(rand(1, 10));

            $keyed = $products->mapWithKeys(function (Product $product) use ($faker) {
                return [$product->getKey() => ['day' => $this->faker->randomElement(DayEnum::values()), 'week' => $this->faker->randomElement(WeekEnum::values()), 'target_id' => Target::query()->inRandomOrder()->first()->getKey()]];
            });

            $mealPack->products()->sync($keyed->toArray());

            $rand = rand(1, 5);

            $mealPack->addMediaFromUrl(asset("img/packs/$rand.png"))->toMediaCollection('pack');
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => ['en' => $this->faker->words(2, true), 'is' => $this->faker->words(2, true)],
            'sub_title' => ['en' => $this->faker->words(2, true), 'is' => $this->faker->words(2, true)],
            'description' => ['en' => $this->faker->realText(200), 'is' => $this->faker->realText(200)],
            'short_description' => ['en' => $this->faker->realText(50), 'is' => $this->faker->realText(50)],
            'ingredients' => ['en' => $this->faker->realText(200), 'is' => $this->faker->realText(200)],
            'allergy' => ['en' => $this->faker->realText(200), 'is' => $this->faker->realText(200)],
            'target_id' => Target::query()->inRandomOrder()->first(),
            'price' => $this->faker->randomNumber(4),
            'price_meal' => $this->faker->randomNumber(3),
            'kcal' => $this->faker->randomNumber(4),
            'is_public' => $this->faker->boolean,
            'per_type' => $this->faker->boolean,
            'nutritional' => [
                'energy' => $this->faker->randomNumber(2),
                'fat' => $this->faker->randomFloat(1, 0, 10),
                'protein' => $this->faker->randomFloat(1, 0, 10),
                'carbohydrates' => $this->faker->randomFloat(1, 0, 10),
            ],
        ];
    }
}
