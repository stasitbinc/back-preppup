<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\ProductSize;
use App\Models\ProductType;
use Illuminate\Database\Eloquent\Factories\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (Product $product) {
            $rand = rand(1, 2);

//    $sizes = ProductSize::all()->random($rand)
//        ->mapWithKeys(function (ProductSize $productSize) use ($faker) {
//            return [$productSize->getKey() => [
//                'dish' => [
//                    'energy' => $this->faker->randomNumber(2),
//                    'fat' => $this->faker->randomFloat(1, 0, 10),
//                    'protein' => $this->faker->randomFloat(1, 0, 10),
//                    'carbohydrates' => $this->faker->randomFloat(1, 0, 10),
//                ],
//                'sauce' => [
//                    'energy' => $this->faker->randomNumber(2),
//                    'fat' => $this->faker->randomFloat(1, 0, 10),
//                    'protein' => $this->faker->randomFloat(1, 0, 10),
//                    'carbohydrates' => $this->faker->randomFloat(1, 0, 10),
//                ],
//            ]];
//        })->toArray();

//    $product->product_sizes()->syncWithoutDetaching($sizes);

            $product->addMediaFromUrl(asset("img/products/$rand.png"))->toMediaCollection('product');

            for ($i = 1; $i <= $rand; $i++) {
                $product->addMediaFromUrl(asset("img/products/$i.png"))->toMediaCollection('product_gallery');
            }
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => ['en' => $this->faker->words(3, true), 'is' => $this->faker->words(3, true)],
            'ingredients' => ['en' => $this->faker->realText(200), 'is' => $this->faker->realText(200)],
            'allergy' => ['en' => $this->faker->realText(200), 'is' => $this->faker->realText(200)],
            'product_type_id' => ProductType::query()->inRandomOrder()->first(),
            'price' => $this->faker->randomNumber(4),
        ];
    }
}
