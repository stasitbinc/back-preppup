<?php

namespace Database\Factories;

use App\Enums\DeliveryEnum;
use App\Enums\OrderStatusEnum;
use App\Enums\PaymentTypeEnum;
use App\Enums\ReservationOrderEnum;
use App\Models\Allergy;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Postcode;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::query()->inRandomOrder()->first(),
            'coupon_id' => $this->faker->boolean(50) ? Coupon::query()->inRandomOrder()->value('id') : null,
            'overview' => [
                'done' => $this->faker->boolean(50),
                'reservations' => $this->faker->randomElement(ReservationOrderEnum::values()),
                'pay_difference' => $this->faker->boolean(50),
            ],
            'allergy' => [
                'done' => $this->faker->boolean(50),
                'enabled' => $this->faker->boolean(50),
                'allergies' => Allergy::query()->inRandomOrder()->limit($this->faker->numberBetween(1, Allergy::query()->count()))->get()->pluck('id'),
                'intolerance' => $this->faker->realText(50, 2),
            ],
            'delivery' => [
                'done' => $this->faker->boolean(50),
                'if' => $this->faker->realText(50, 2),
                'date' => $this->faker->date('Y-m-d'),
                'type' => DeliveryEnum::DAY,
            ],
            'address' => [
                'done' => $this->faker->boolean(50),
                'is_same' => $this->faker->boolean(50),
                'city' => $this->faker->city,
                'floor' => $this->faker->secondaryAddress,
                'address' => $this->faker->streetAddress,
                'postcode_id' => Postcode::query()->inRandomOrder()->first()->getKey(),
            ],
            'payment' => [
                'done' => $this->faker->boolean(50),
                'type' => PaymentTypeEnum::KORTA,
                'korta' => [
                    'card4' => $this->faker->randomNumber(4),
                    'cardbrand' => $this->faker->creditCardType,
                    'time' => $this->faker->date('d-m-y H:i:s'),
                    'token' => strtolower(Str::random(19)),
                    'authcode' => $this->faker->ean8,
                    'reference' => $this->faker->isbn10,
                    'downloadmd5' => $this->faker->md5,
                    'transaction' => $this->faker->ean8,
                ],
            ],
            'step' => $this->faker->randomElement(['overview', 'allergy', 'delivery', 'address', 'payment']),
            'status' => $this->faker->randomElement(OrderStatusEnum::values()),
            'date' => $this->faker->dateTimeBetween('-1 years', 'now'),
            'amount' => [
                'total' => $this->faker->randomNumber(4),
                'coupon' => $this->faker->randomNumber(3),
                'shipping' => Postcode::query()->inRandomOrder()->first()->getAttribute('standard'),
                'difference' => $this->faker->randomNumber(3),
                'for_payment' => $this->faker->randomNumber(4),
            ],
            'created_at' => $this->faker->dateTimeBetween('-2 years', 'now'),
            'updated_at' => $this->faker->dateTimeBetween('-2 years', 'now'),
        ];
    }
}
