<?php

namespace Database\Factories;

use App\Models\FaqCategory;
use App\Models\Term;
use Illuminate\Database\Eloquent\Factories\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class TermFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Term::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => ['en' => $this->faker->words(2, true), 'is' => $this->faker->words(2, true)],
            'answer' => ['en' => $this->faker->realText(50), 'is' => $this->faker->realText(50)],
        ];
    }
}
