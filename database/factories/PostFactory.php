<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\Target;
use Illuminate\Database\Eloquent\Factories\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (Post $post) {
            $rand = rand(1, 3);

            $post->addMediaFromUrl(asset("img/posts/$rand.png"))->toMediaCollection('post');
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => ['en' => $this->faker->words(2, true), 'is' => $this->faker->words(2, true)],
            'description' => ['en' => $this->faker->realText(200), 'is' => $this->faker->realText(200)],
            'name' => ['en' => $this->faker->name, 'is' => $this->faker->name],
            'target_id' => Target::query()->inRandomOrder()->first(),
        ];
    }
}
