<?php

namespace Database\Factories;

use App\Models\SubscriptionPlan;
use Illuminate\Database\Eloquent\Factories\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class SubscriptionPlanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubscriptionPlan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'days' => $this->faker->randomNumber(2),
            'one_meal' => $this->faker->randomNumber(4),
            'two_meal' => $this->faker->randomNumber(4),
            'three_meal' => $this->faker->randomNumber(4),
            'hours_to_save' => $this->faker->randomNumber(1),
        ];
    }
}
