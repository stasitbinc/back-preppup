<?php

namespace Database\Factories;

use App\Models\Faq;
use App\Models\FaqCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class FaqFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Faq::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'question' => ['en' => $this->faker->words(2, true), 'is' => $this->faker->words(2, true)],
            'answer' => ['en' => $this->faker->realText(50), 'is' => $this->faker->realText(50)],
            'faq_category_id' => FaqCategory::query()->inRandomOrder()->first(),
        ];
    }
}
