<?php

namespace Database\Factories;

use App\Enums\SubscriptionStatusEnum;
use App\Models\Buyable;
use App\Models\Order;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class SubscriptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Subscription::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::query()->inRandomOrder()->first(),
            'buyable_id' => Buyable::query()->inRandomOrder()->first(),
            'order_id' => Order::query()->inRandomOrder()->first(),
            'status' => $this->faker->randomElement(SubscriptionStatusEnum::values()),
        ];
    }
}
